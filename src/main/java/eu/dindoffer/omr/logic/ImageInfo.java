package eu.dindoffer.omr.logic;

/**
 *
 * @author Martin Dindoffer
 */
public class ImageInfo {

    public final int width, height;

    public ImageInfo(int width, int height) {
        this.width = width;
        this.height = height;
    }
}
