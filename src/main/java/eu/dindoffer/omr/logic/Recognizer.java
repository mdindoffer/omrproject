package eu.dindoffer.omr.logic;

import static org.opencv.imgcodecs.Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE;
import static org.opencv.imgcodecs.Imgcodecs.imread;

import eu.dindoffer.omr.logic.dataset.BGDatasetGenerator;
import eu.dindoffer.omr.logic.dataset.PlainNoteHeadSampleEvaluator;
import eu.dindoffer.omr.logic.dataset.SuccessEvaluator;
import eu.dindoffer.omr.logic.notehead.NoteHeadSVM;
import eu.dindoffer.omr.logic.util.Utils;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import org.opencv.core.Mat;

/**
 *
 * @author Martin Dindoffer
 */
public class Recognizer {

    private static Recognizer self;

    private Page currentPage;

    private Recognizer() {
    }

    public static synchronized Recognizer getInstance() {
        if (self == null) {
            self = new Recognizer();
        }
        return self;
    }

    public void runOperation(Operation operation) {
        switch (operation) {
            case BINARIZE:
                binarizeWithThreshold(Settings.binarizationThreshold);
                break;
            case DENOISE:
                morphologySmoothing(Settings.openingFirst, Settings.openingMaskSize, Settings.closingMaskSize);
                break;
            case PROJECT_HISTOGRAMS:
                calculateProjectionHistograms();
                break;
            case CALCULATE_VERTICAL_LENGTHS:
                calcVerticalLengthHistograms(Settings.maxVerticalLengthToTrack);
                break;
            case HOUGH_TRANSFORM:
                probabilisticHough();
                break;
            case SKELETONIZE:
                skeletonizeExtractedLines();
                break;
            case FIND_POLYLINES:
                findPolyLines();
                break;
            case FIND_STAFF_LINES:
                findStaffLines();
                break;
            case REMOVE_STAFF_LINES:
                removeStaffLines();
                break;
            case DETECT_NOTE_HEADS:
                detectNoteHeads();
                break;
            case UNIFY_NH_ANNOTATIONS:
                unifyNoteHeadAnnotations();
                break;
            case RESOLVE_NH_MULTIPLICITIES:
                resolveNoteHeadMultiplicities();
                break;
            default:
                throw new IllegalArgumentException(operation.name());
        }
    }

    public ImageInfo loadImage(File fImg) throws IllegalArgumentException {
        Mat loadedMat = imread(fImg.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
        currentPage = new Page(loadedMat);
        return new ImageInfo(loadedMat.width(), loadedMat.height());
    }

    public void binarizeWithThreshold(int threshold) {
        currentPage.binarizeWithThreshold(threshold);
    }

    public BufferedImage getDisplayableVisualResult() {
        return Utils.toBufferedImage(currentPage.getVisualResultPage());
    }

    public int[] getBlackVerticalLengthHistogram() {
        return currentPage.getBlackVerticalLengthHistogram();
    }

    public int[] getWhiteVerticalLengthHistogram() {
        return currentPage.getWhiteVerticalLengthHistogram();
    }

    public BufferedImage getDisplayableOriginal() {
        return Utils.toBufferedImage(currentPage.getOriginal());
    }

    public int getOriginalWidth() {
        return currentPage.getOriginal().width();
    }

    public int getOriginalHeight() {
        return currentPage.getOriginal().height();
    }

    public void morphologySmoothing(boolean openingFirst, int openingMaskSize, int closingMaskSize) {
        currentPage.morphologySmoothing(openingFirst, openingMaskSize, closingMaskSize);
    }

    /**
     * Calculates horizontal and vertical projection histograms of current page.
     */
    public void calculateProjectionHistograms() {
        currentPage.calculateProjectionHistograms();
    }

    /**
     * Calculates histograms of lengths of continuous vertical runs in the current page. As a side product a modus of staff
     * space height is calculated.
     *
     * @param maxLengthToTrack maximum length in pixels to track
     */
    public void calcVerticalLengthHistograms(int maxLengthToTrack) {
        currentPage.calcVerticalLengthHistograms(maxLengthToTrack);
    }

    /**
     * Performs a probabilistic Hough algorithm on the (scaled down) current page to extract horizontal line segments.
     */
    public void probabilisticHough() {
        currentPage.probabilisticHough();
    }

    /**
     * Skeletonizes the extracted horizontal line segments using Zhang-Suen algorithm.
     */
    public void skeletonizeExtractedLines() {
        currentPage.skeletonizeExtractedLines();
    }

    /**
     * Finds the polylines in the skeletonized image by manual tracing of the line segments and finding the shortest (in
     * terms of the number of line segments) horizontal path using Dijkstra.
     */
    public void findPolyLines() {
        currentPage.findPolyLines();
    }

    public void findStaffLines() {
        currentPage.findStaffLines();
    }

    public void removeStaffLines() {
        currentPage.removeStaffLines();
    }

    public void trainNoteHeadSVM(File trainDir) throws IOException {
        NoteHeadSVM.getInstance().trainNoteHeadSVM(trainDir);
    }

    public void detectNoteHeads() {
        currentPage.detectNoteHeads(true, true);
    }

    public void createBGDataset(File annotations, int expectedAmountOfSamples) {
        BGDatasetGenerator generator = new BGDatasetGenerator(expectedAmountOfSamples);
        generator.createBGDataset(annotations);
    }

    public String calculateNoteHeadSVMSuccesRate(File annotations) {
        return new SuccessEvaluator(annotations).calculateNoteHeadSVMSuccesRate();
    }

    public String evaluatePlainNoteHeadSamples(File chosenDir) throws IOException {
        return new PlainNoteHeadSampleEvaluator().evaluate(chosenDir);
    }

    public void unifyNoteHeadAnnotations() {
        currentPage.unifyNoteHeadAnnotations();
    }

    public void resolveNoteHeadMultiplicities() {
        currentPage.resolveMultipleNoteHeads();
    }
}
