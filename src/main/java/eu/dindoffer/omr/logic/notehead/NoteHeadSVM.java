package eu.dindoffer.omr.logic.notehead;

import static org.opencv.imgcodecs.Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE;

import eu.dindoffer.omr.logic.util.SVMTuplet;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.ml.Ml;
import org.opencv.ml.SVM;

/**
 *
 * @author Martin Dindoffer
 */
public class NoteHeadSVM {

    private static NoteHeadSVM self;

    private SVM svm;
    private final File svmModel = new File(System.getProperty("user.dir"), "svm-models" + File.separator + "noteHeadModel.xml");

    private NoteHeadSVM() {
        this.svm = SVM.load(svmModel.getAbsolutePath());
        this.svm.setType(SVM.C_SVC);
        this.svm.setKernel(SVM.LINEAR);
    }

    public void reloadModel() {
        this.svm = SVM.load(svmModel.getAbsolutePath());
        this.svm.setType(SVM.C_SVC);
        this.svm.setKernel(SVM.LINEAR);
    }

    public static NoteHeadSVM getInstance() {
        if (self == null) {
            self = new NoteHeadSVM();
        }
        return self;
    }

    public void trainNoteHeadSVM(File trainDir) throws IOException {
        SVMTuplet svmTuplet = loadNoteHeadSamples(trainDir);
        System.out.println("Starting SVM training");
        svm.trainAuto(svmTuplet.getSamplesMat(), Ml.ROW_SAMPLE , svmTuplet.getLabelsMat());
        System.out.println("SVM training completed");
        svm.save(new File(trainDir, "noteHeadModel.xml").getAbsolutePath());
        svm.save(svmModel.getAbsolutePath());
    }

    /**
     *
     * @param samples a Mat of samples to predict, one row == one sample, 900 columns wide, 32fC1
     * @param annotations a list of NoteHeadAnnotation objects to set the results of the predictions to
     */
    public void predictNoteHeads(Mat samples, List<NoteHeadAnnotation> annotations) {
        Mat results = predictNoteHeads(samples);
        System.out.println("RESULTS MAT>" + results.toString());
        System.out.println("RESULTS>\n");

        ListIterator<NoteHeadAnnotation> annotationIterator = annotations.listIterator();
        for (int j = 0; j < results.height(); j++) {
            float[] data = new float[1];
            results.get(j, 0, data);
            annotationIterator.next().setType(NoteHeadType.getById((int) data[0]));
        }
    }

    public Mat predictNoteHeads(Mat samples) {
        Mat results = new Mat();
        svm.predict(samples, results, 0);
        return results;
    }

    public SVMTuplet loadNoteHeadSamples(File trainDir) throws IOException {
        File quarterDir = new File(trainDir, "quarternotehead");
        File halfDir = new File(trainDir, "halfnotehead");
        File wholeDir = new File(trainDir, "wholenotehead");
        File bgDir = new File(trainDir, "background");
        if (!quarterDir.exists() || !halfDir.exists() || !wholeDir.exists() || !bgDir.exists()) {
            throw new IOException("Not a valid directory for NoteHead training set");
        }
        File quarterImages[] = quarterDir.listFiles();
        File halfImages[] = halfDir.listFiles();
        File wholeImages[] = wholeDir.listFiles();
        File bgImages[] = bgDir.listFiles();
        List<Mat> loadedMats = new LinkedList<>();
        Mat labelsMat = new Mat(quarterImages.length + halfImages.length + wholeImages.length + bgImages.length,
                1, CvType.CV_32FC1);
        int imgNum = 0;
        System.out.println("Loading quarter note samples...");
        for (File f : quarterImages) {
            Mat loadedMat = Imgcodecs.imread(f.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
            Mat reshapedMat = loadedMat.reshape(0, 1);
            loadedMats.add(reshapedMat);
            labelsMat.put(imgNum++, 0, NoteHeadType.QUARTER_HEAD.getId());
        }
        System.out.println("Loading half note samples...");
        for (File f : halfImages) {
            Mat loadedMat = Imgcodecs.imread(f.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
            Mat reshapedMat = loadedMat.reshape(0, 1);
            loadedMats.add(reshapedMat);
            labelsMat.put(imgNum++, 0, NoteHeadType.HALF_HEAD.getId());
        }
        System.out.println("Loading whole note samples...");
        for (File f : wholeImages) {
            Mat loadedMat = Imgcodecs.imread(f.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
            Mat reshapedMat = loadedMat.reshape(0, 1);
            loadedMats.add(reshapedMat);
            labelsMat.put(imgNum++, 0, NoteHeadType.WHOLE_HEAD.getId());
        }
        System.out.println("Loading background samples...");
        for (File f : bgImages) {
            Mat loadedMat = Imgcodecs.imread(f.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
            Mat reshapedMat = loadedMat.reshape(0, 1);
            loadedMats.add(reshapedMat);
            labelsMat.put(imgNum++, 0, NoteHeadType.BACKGROUND.getId());
        }
        Mat trainingMat = new Mat();
        Core.vconcat(loadedMats, trainingMat);
        trainingMat.convertTo(trainingMat, CvType.CV_32FC1);
        return new SVMTuplet(trainingMat, labelsMat);
    }
}
