package eu.dindoffer.omr.logic.notehead;

import eu.dindoffer.omr.logic.util.EnhancedRectangle;
import org.opencv.core.Point;

/**
 *
 * @author Martin Dindoffer
 */
public class NoteHeadAnnotation {

    private EnhancedRectangle bBoxHighDPI;
    private Point topLeftHighDPI, bottomRightHighDPI;
    private Point topLeftLowDPI, bottomRightLowDPI;
    private NoteHeadType type;

    public NoteHeadAnnotation(EnhancedRectangle bboxHighDPI, NoteHeadType type) {
        this.bBoxHighDPI = bboxHighDPI;
        this.type = type;
    }

    public NoteHeadAnnotation(Point topLeftHighDPI, Point bottomRightHighDPI, Point topLeftLowDPI, Point bottomRightLowDPI) {
        this.topLeftHighDPI = topLeftHighDPI;
        this.bottomRightHighDPI = bottomRightHighDPI;
        this.topLeftLowDPI = topLeftLowDPI;
        this.bottomRightLowDPI = bottomRightLowDPI;
        int bboxWidthHighDPI = (int) (bottomRightHighDPI.x - topLeftHighDPI.x);
        int bboxHeightHighDPI = (int) (bottomRightHighDPI.y - topLeftHighDPI.y);
        this.bBoxHighDPI = new EnhancedRectangle((int) topLeftHighDPI.x, (int) topLeftHighDPI.y, bboxWidthHighDPI,
                bboxHeightHighDPI);
    }

    public Point getTopLeftLowDPI() {
        return topLeftLowDPI;
    }

    public void setTopLeftLowDPI(Point topLeftLowDPI) {
        this.topLeftLowDPI = topLeftLowDPI;
    }

    public EnhancedRectangle getBBoxHighDPI() {
        return bBoxHighDPI;
    }

    public void setBBoxHighDPI(EnhancedRectangle bBoxHighDPI) {
        this.bBoxHighDPI = bBoxHighDPI;
    }

    public Point getBottomRightLowDPI() {
        return bottomRightLowDPI;
    }

    public void setBottomRightLowDPI(Point bottomRightLowDPI) {
        this.bottomRightLowDPI = bottomRightLowDPI;
    }

    public void setType(NoteHeadType type) {
        this.type = type;
    }

    public Point getTopLeftHighDPI() {
        return topLeftHighDPI;
    }

    public Point getBottomRightHighDPI() {
        return bottomRightHighDPI;
    }

    public NoteHeadType getType() {
        return type;
    }
}
