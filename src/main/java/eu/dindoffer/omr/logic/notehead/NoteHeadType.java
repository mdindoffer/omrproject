package eu.dindoffer.omr.logic.notehead;

import java.util.HashMap;
import java.util.Map;
import org.opencv.core.Scalar;

/**
 *
 * @author Martin Dindoffer
 */
public enum NoteHeadType {
    INDETERMINISTIC(-2, new Scalar(0, 255, 255)), BACKGROUND(-1, new Scalar(0, 0, 0)),
    QUARTER_HEAD(1, new Scalar(255, 0, 0)),
    HALF_HEAD(2, new Scalar(0, 255, 0)),
    WHOLE_HEAD(3, new Scalar(0, 0, 255));

    private static final Map<Integer, NoteHeadType> TYPES_BY_ID = new HashMap<>();
    private final int id;
    private final Scalar bboxColor;

    private NoteHeadType(int id, Scalar bboxColor) {
        this.id = id;
        this.bboxColor = bboxColor;
    }

    static {
        NoteHeadType vals[] = NoteHeadType.values();
        for (NoteHeadType val : vals) {
            TYPES_BY_ID.put(val.id, val);
        }
    }

    public int getId() {
        return id;
    }

    public static NoteHeadType getById(int id) {
        return TYPES_BY_ID.get(id);
    }

    public Scalar getBboxColor() {
        return bboxColor;
    }
}
