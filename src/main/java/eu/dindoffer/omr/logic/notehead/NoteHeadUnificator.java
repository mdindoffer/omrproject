package eu.dindoffer.omr.logic.notehead;

import eu.dindoffer.omr.logic.Settings;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Martin Dindoffer
 */
public class NoteHeadUnificator {

    private List<NoteHeadAnnotation> quarters = new LinkedList<>();
    private List<NoteHeadAnnotation> halves = new LinkedList<>();
    private List<NoteHeadAnnotation> wholes = new LinkedList<>();

    public List<NoteHead> unifyNoteHeadAnnotations(List<NoteHeadAnnotation> noteHeadAnnotations) {
        //sort the annotations by their type
        sortAnnotations(noteHeadAnnotations);
        List<NoteHead> quarterNoteHeads = unifyAnnotations(quarters);
        List<NoteHead> halfNoteHeads = unifyAnnotations(halves);
        List<NoteHead> wholeNoteHeads = unifyAnnotations(wholes);
        List<NoteHead> allNoteHeads = new LinkedList<>();
        allNoteHeads.addAll(quarterNoteHeads);
        allNoteHeads.addAll(halfNoteHeads);
        allNoteHeads.addAll(wholeNoteHeads);
        return allNoteHeads;
    }

    //TODO: split into more methods
    public List<NoteHead> unifyNoteHeads(List<NoteHead> noteHeads, boolean sameClassUnification) {
        ListIterator<NoteHead> it = noteHeads.listIterator();
        boolean modified = false;
        while (it.hasNext()) {
            if (modified) {
                it = noteHeads.listIterator();
                modified = false;
            }
            NoteHead currBase = it.next();
            if (it.hasNext()) {
                ListIterator<NoteHead> it2 = noteHeads.listIterator(it.nextIndex());
                while (it2.hasNext()) {
                    NoteHead currSecond = it2.next();
                    double intersectRatio = currBase.getBbox().intersectionOverUnion(currSecond.getBbox());
                    if (intersectRatio > Settings.unificationThreshold) {
                        if (sameClassUnification) {
                            currBase.absorb(currSecond);
                            it2.remove();
                        } else {
                            if (currBase.getBackedBy() > currSecond.getBackedBy()) {
                                it2.remove();
                            } else if (currBase.getBackedBy() < currSecond.getBackedBy()) {
                                it.remove();
                                modified = true;
                                break;
                            } else {
                                currBase.absorb(currSecond);
                                currBase.setType(NoteHeadType.INDETERMINISTIC);
                            }
                        }
                        modified = true;
                    }
                }
            }
        }
        return noteHeads;
    }

    private List<NoteHead> unifyAnnotations(List<NoteHeadAnnotation> annotations) {
        //create NoteHead candidates as wrappers
        List<NoteHead> noteHeadCandidates = new LinkedList<>();
        for (NoteHeadAnnotation a : annotations) {
            noteHeadCandidates.add(new NoteHead(a));
        }
        return unifyNoteHeads(noteHeadCandidates, true);
    }

    private void sortAnnotations(List<NoteHeadAnnotation> annotations) {
        for (NoteHeadAnnotation annotation : annotations) {
            switch (annotation.getType()) {
                case QUARTER_HEAD:
                    quarters.add(annotation);
                    break;
                case HALF_HEAD:
                    halves.add(annotation);
                    break;
                case WHOLE_HEAD:
                    wholes.add(annotation);
                    break;
            }
        }
    }
}
