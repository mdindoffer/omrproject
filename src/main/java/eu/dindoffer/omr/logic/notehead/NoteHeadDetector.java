package eu.dindoffer.omr.logic.notehead;

import eu.dindoffer.omr.logic.Settings;
import eu.dindoffer.omr.logic.util.BinaryMat;
import eu.dindoffer.omr.logic.util.Utils;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;

/**
 *
 * @author Martin Dindoffer
 */
public class NoteHeadDetector {

    private final double scalingFactor;
    private final int normalizedWindowWidth;

    public NoteHeadDetector(int staffSpaceHeightModus) {
        this.normalizedWindowWidth = 30;
        double nonScaledWindowWidth;
        if (staffSpaceHeightModus > 0) {
            nonScaledWindowWidth = 1.8 * staffSpaceHeightModus;
        } else {
            nonScaledWindowWidth = 1.8 * 35;
        }
        this.scalingFactor = normalizedWindowWidth / nonScaledWindowWidth;
        NoteHeadSVM.getInstance().reloadModel();
    }

    /*
     * OK, so we have a modus of staff space height. A note head is usually wider than taller. Essentially up to 1.8 times
     * wider for the whole note. That means we have to multiply the modus height by that factor and create a square bounding
     * box of that huge height. Then resize it to 30x30 for the SVM to process. Better approach is to calculate the resize
     * factor, resize the whole page and then just cut the images and thats what we'll do. Returns the annotations in
     * original, non-scaled format
     */
    public List<NoteHeadAnnotation> detectNoteHeads(BinaryMat workingMat, boolean discardBGDetections) {
        //if the working mat has a white foreground, we have to invert it
        if (!workingMat.hasBlackForeground()) {
            workingMat.invert();
        }
        BinaryMat bResizedMat = workingMat.createResizedCopy(scalingFactor);
        List<Mat> cutoutMats = new ArrayList<>();
        List<NoteHeadAnnotation> annotations = new LinkedList<>();
        Long start = System.currentTimeMillis();
        pinpointCandidates(bResizedMat.getMat(), cutoutMats, annotations);
        System.out.println("Duration of candidate pinpointing: " + (System.currentTimeMillis() - start) + "ms");
        Mat samples = new Mat();
        Core.vconcat(cutoutMats, samples);
        System.out.println("pinpointing + vconcat: " + (System.currentTimeMillis() - start) + "ms");
        samples.convertTo(samples, CvType.CV_32FC1);
        System.out.println("pinpointing + vconcat + convertTo: " + (System.currentTimeMillis() - start) + "ms");
        System.out.println("Starting prediction");
        long startOfPrediction = System.currentTimeMillis();
        NoteHeadSVM.getInstance().predictNoteHeads(samples, annotations);
        System.out.println("Finished predition");
        System.out.println("Duration of prediction: " + (System.currentTimeMillis() - startOfPrediction));
        if (discardBGDetections) {
            System.out.println("Removing background annotations from the SVM results...");
            annotations.removeIf((NoteHeadAnnotation t) -> {
                return t.getType() == NoteHeadType.BACKGROUND;
            });
            System.out.println("Done");
        }
        return annotations;
    }

    private void pinpointCandidates(Mat resizedMat, List<Mat> cutoutMats, List<NoteHeadAnnotation> annotations) {
        int skippedROIsCount = 0;
        int parsingWidthLimit = resizedMat.width() - normalizedWindowWidth;
        int parsingHeightLimit = resizedMat.height() - normalizedWindowWidth;
        double windowArrea = normalizedWindowWidth * normalizedWindowWidth;
        for (int col = 0; col < parsingWidthLimit; col += Settings.windowingStepSize) {
            for (int row = 0; row < parsingHeightLimit; row += Settings.windowingStepSize) {
                Point topLeft = new Point(col, row);
                int bottomRightX = col + normalizedWindowWidth;
                int bottomRightY = row + normalizedWindowWidth;
                Point bottomRight = new Point(bottomRightX, bottomRightY);
                Mat roiMat = resizedMat.submat(row, bottomRightY, col, bottomRightX);
                // if we have a window consisting of at least 5% of black pixels, we add it to detection
                if ((Core.countNonZero(roiMat) / windowArrea) < 0.95) {
                    Mat reshapedCutout = roiMat.clone().reshape(0, 1);
                    cutoutMats.add(reshapedCutout);
                    Point ldpiTopLeft = topLeft.clone();
                    Point ldpiBotRight = bottomRight.clone();
                    Utils.scalePointUp(topLeft, scalingFactor);
                    Utils.scalePointUp(bottomRight, scalingFactor);
                    annotations.add(new NoteHeadAnnotation(topLeft, bottomRight, ldpiTopLeft, ldpiBotRight));
                } else {
                    skippedROIsCount++;
                }
            }
        }
        System.out.println("Skipped ROIs>" + skippedROIsCount);
    }

    public double getScalingFactor() {
        return scalingFactor;
    }
}
