package eu.dindoffer.omr.logic.notehead;

import eu.dindoffer.omr.logic.util.EnhancedRectangle;

/**
 *
 * @author Martin Dindoffer
 */
public class NoteHead {

    private EnhancedRectangle bbox;
    private NoteHeadType type;
    private int backedBy;

    public NoteHead(EnhancedRectangle bbox, NoteHeadType type, int backedBy) {
        this.bbox = bbox;
        this.type = type;
        this.backedBy = backedBy;
    }

    public NoteHead(NoteHeadAnnotation nha) {
        this.bbox = nha.getBBoxHighDPI();
        this.type = nha.getType();
        this.backedBy = 1;
    }

    public EnhancedRectangle getBbox() {
        return bbox;
    }

    public NoteHeadType getType() {
        return type;
    }

    public void setType(NoteHeadType type) {
        this.type = type;
    }

    public int getBackedBy() {
        return backedBy;
    }

    public void absorb(NoteHead another) {
        this.bbox = this.bbox.union(another.bbox);
        this.backedBy += another.backedBy;
    }
}
