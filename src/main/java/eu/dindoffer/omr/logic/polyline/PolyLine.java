package eu.dindoffer.omr.logic.polyline;

import static eu.dindoffer.omr.logic.util.Utils.arePointsAdjacent;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.Multigraph;
import org.opencv.core.Point;

/**
 *
 * @author Martin Dindoffer
 */
public class PolyLine {

    private List<PolyLineSegment> polyLineSegments;
    private int avgY;

    public List<PolyLineSegment> getPolyLineSegments() {
        return polyLineSegments;
    }

    public int getAvgY() {
        return avgY;
    }

    public PolyLine(List<PolyLineSegment> polyLineSegments) {
        this.polyLineSegments = polyLineSegments;
    }

    public int calculateWidth() {
        PolyLineSegment[] endpointSegments = findLeftAndRightEndPointSegments();
        return (int) (endpointSegments[1].getRightLowerPoint().x - endpointSegments[0].getLeftUpperPoint().x);
    }

    /**
     * Calculates average Y value based on ends of its segments.
     */
    public void calcAvgY() {
        int sum = 0;
        for (PolyLineSegment pls : polyLineSegments) {
            sum += pls.getLeftUpperPoint().y + pls.getRightLowerPoint().y;
        }
        avgY = sum / (polyLineSegments.size() * 2);
    }

    /**
     * Trims secondary branches via backtracking to leave the longest horizontal path.
     */
    public void comb() {
        if (polyLineSegments.size() == 1) {
            return;
        }
        //sort the segments from upper left to lower right
        polyLineSegments.sort((PolyLineSegment pls1, PolyLineSegment pls2) -> {
            double x = pls1.getLeftUpperPoint().x - pls2.getLeftUpperPoint().x;
            if (x < 0) {
                return -1;
            }
            if (x > 0) {
                return 1;
            }
            double y = pls1.getLeftUpperPoint().y - pls2.getLeftUpperPoint().y;
            if (y < 0) {
                return -1;
            }
            if (y > 0) {
                return 1;
            }
            return 0;
        });

        //now we find a segment containing the rightmost point in the polyLine
        PolyLineSegment rightEnd = polyLineSegments.get(0);
        for (PolyLineSegment pls : polyLineSegments) {
            if (pls.getRightLowerPoint().x > rightEnd.getRightLowerPoint().x) {
                rightEnd = pls;
            }
        }

        //find longest path
        LinkedList<PolyLineSegment> longestPathSegments = new LinkedList<>();
        longestPathSegments.add(polyLineSegments.get(0));
        int index = 0;
        while (true) {
            index++;
            Point rightP = longestPathSegments.getLast().getRightLowerPoint();
            Point leftP = polyLineSegments.get(index).getLeftUpperPoint();
            if (arePointsAdjacent(rightP, leftP)) {
                longestPathSegments.add(polyLineSegments.get(index));
                if (longestPathSegments.getLast() == rightEnd) {//until we have added the rightmost segment
                    break;
                }
            }
            while (index == polyLineSegments.size() - 1) { //we have stumbled upon a dead end
                if (longestPathSegments.size() > 1) {
                    polyLineSegments.remove(longestPathSegments.getLast()); // remove the dead segment at the end
                    longestPathSegments.removeLast();//also remove the dead path segment
                }
                //and reset the index to last position in current path
                index = polyLineSegments.indexOf(longestPathSegments.getLast());
            }
        }
        polyLineSegments = longestPathSegments;
    }

    /**
     * Finds the shortest (simplest) path from left to right end of the polyline using dijsktra.
     */
    public void combByDijkstra() {
        //we find segments containing the rightmost and leftmost point in the polyLine
        PolyLineSegment[] endpointSegments = findLeftAndRightEndPointSegments();
        if (endpointSegments[0] == endpointSegments[1]) {//in case we have one long segment
            polyLineSegments.clear();//we can end prematurely
            polyLineSegments.add(endpointSegments[0]);
            return;
        }

        Multigraph<PolyLineSegment, Object> graph = new Multigraph<>(Object.class);
        for (PolyLineSegment pls : polyLineSegments) {
            graph.addVertex(pls);
        }
        ListIterator<PolyLineSegment> forwardsIterator = polyLineSegments.listIterator(0);
        ListIterator<PolyLineSegment> backwardsIterator;
        while (forwardsIterator.nextIndex() != polyLineSegments.size() - 1) {//iterate excluding the last segment
            PolyLineSegment firstSegment = forwardsIterator.next();
            backwardsIterator = polyLineSegments.listIterator(polyLineSegments.size());
            while (backwardsIterator.previousIndex() != forwardsIterator.previousIndex()) {//iterate backwards until we meet
                PolyLineSegment secondSegment = backwardsIterator.previous();
                if (firstSegment.isAttachedTo(secondSegment)) {
                    graph.addEdge(firstSegment, secondSegment);
                }
            }
        }

        ShortestPathAlgorithm<PolyLineSegment, Object> dijkstraAlg = new DijkstraShortestPath<>(graph);
        GraphPath<PolyLineSegment, Object> path = dijkstraAlg.getPath(endpointSegments[0], endpointSegments[1]);
        polyLineSegments = path.getVertexList();
    }

    /**
     * Finds the leftmost and rightmost segments of the polyline.
     *
     * @return array of [leftmost,rightmost] segments
     */
    public PolyLineSegment[] findLeftAndRightEndPointSegments() {
        PolyLineSegment rightEnd = polyLineSegments.get(0);
        PolyLineSegment leftEnd = polyLineSegments.get(0);
        for (PolyLineSegment pls : polyLineSegments) {
            if (pls.getRightLowerPoint().x > rightEnd.getRightLowerPoint().x) {
                rightEnd = pls;
            }
            if (pls.getLeftUpperPoint().x < leftEnd.getLeftUpperPoint().x) {
                leftEnd = pls;
            }
        }
        PolyLineSegment[] array = new PolyLineSegment[2];
        array[0] = leftEnd;
        array[1] = rightEnd;
        return array;
    }

    @Override
    public String toString() {
        return polyLineSegments.toString();
    }
}
