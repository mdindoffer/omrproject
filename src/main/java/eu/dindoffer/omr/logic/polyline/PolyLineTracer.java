package eu.dindoffer.omr.logic.polyline;

import static eu.dindoffer.omr.logic.util.ByteColors.BYTE_BLACK;
import static eu.dindoffer.omr.logic.util.ByteColors.BYTE_WHITE;
import static org.opencv.core.Core.BORDER_CONSTANT;

import eu.dindoffer.omr.logic.util.Utils;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;

/**
 *
 * @author Martin Dindoffer
 */
public class PolyLineTracer {

    /**
     * Traces individual line segments from the extracted lines of the current page.
     *
     * @return line segments
     */
    public List<PolyLineSegment> tracePolyLineSegments(Mat extractedLines) {
        List<PolyLineSegment> segmentList = new LinkedList<>();
        //we pad the image with black border to avoid index overflow checking later on
        Mat paddedImg = new Mat();
        Core.copyMakeBorder(extractedLines, paddedImg, 1, 1, 1, 1, BORDER_CONSTANT, new Scalar(BYTE_BLACK.getCode()));
        byte[][] extractedLinesArr = Utils.mat8UC1ToByteArrayYX(paddedImg);
        //we iterate over the image looking for horizontal, vertical and diagonal segments
        traceHorizontalPolyLineSegments(extractedLinesArr, segmentList);
        traceVerticalPolyLineSegments(extractedLinesArr, segmentList);
        traceDiagonalDownwardsPolyLineSegments(extractedLinesArr, segmentList);
        traceDiagonalUpwardsPolyLineSegments(extractedLinesArr, segmentList);
        return segmentList;
    }

    private List<PolyLineSegment> traceDiagonalUpwardsPolyLineSegments(byte[][] extractedLinesArr,
            List<PolyLineSegment> segmentList) {
        //copy the image to allow changes
        byte[][] extractedLinesArrCpy = new byte[extractedLinesArr.length][];
        for (int i = 0; i < extractedLinesArr.length; i++) {
            extractedLinesArrCpy[i] = Arrays.copyOf(extractedLinesArr[i], extractedLinesArr[i].length);
        }
        extractedLinesArr = extractedLinesArrCpy;

        //for each row from the bottom
        for (int y = extractedLinesArr.length - 1; y >= 0; y--) {
            int x = 0;
            //until the end of the row
            while (x < extractedLinesArr[0].length) {
                //we skip black pixels until end of the row
                while (x < extractedLinesArr[0].length && extractedLinesArr[y][x] == BYTE_BLACK.getCode()) {
                    x++;
                }
                //if we are at the end, move on to the next row
                if (x >= extractedLinesArr[0].length) {
                    break;
                }
                int xStartOfSegment = x;
                int yStartOfSegment = y;
                //if the next pixel is white
                while (extractedLinesArr[y - 1][x + 1] == BYTE_WHITE.getCode()) {//TODO: index overflow check
                    x++;//then step on it
                    y--;
                    //if the pixel has upper or right white neighbour, we stop current segment tracing
                    if (extractedLinesArr[y - 1][x] == BYTE_WHITE.getCode()//TODO: index overflow check
                            || extractedLinesArr[y][x + 1] == BYTE_WHITE.getCode()) {
                        break;
                    } else {
                        //otherwise make it black/visited
                        extractedLinesArr[y][x] = BYTE_BLACK.getCode();
                    }
                }
                if (x > xStartOfSegment) {
                    segmentList.add(new PolyLineSegment(new Point(xStartOfSegment - 1, yStartOfSegment - 1), new Point(x - 1,
                            y - 1)));
                }
                //we step on the next pixel
                x = xStartOfSegment + 1;
                y = yStartOfSegment;
            }
        }
        return segmentList;
    }

    private List<PolyLineSegment> traceDiagonalDownwardsPolyLineSegments(byte[][] extractedLinesArr,
            List<PolyLineSegment> segmentList) {
        //copy the image to allow changes
        byte[][] extractedLinesArrCpy = new byte[extractedLinesArr.length][];
        for (int i = 0; i < extractedLinesArr.length; i++) {
            extractedLinesArrCpy[i] = Arrays.copyOf(extractedLinesArr[i], extractedLinesArr[i].length);
        }
        extractedLinesArr = extractedLinesArrCpy;

        //for each row
        for (int y = 0; y < extractedLinesArr.length; y++) {
            int x = 0;
            //until the end of the row
            while (x < extractedLinesArr[0].length) {
                //we skip black pixels until end of the row
                while (x < extractedLinesArr[0].length && extractedLinesArr[y][x] == BYTE_BLACK.getCode()) {
                    x++;
                }
                //if we are at the end, move on to the next row
                if (x >= extractedLinesArr[0].length) {
                    break;
                }
                int xStartOfSegment = x;
                int yStartOfSegment = y;
                //if the next pixel is white
                while (extractedLinesArr[y + 1][x + 1] == BYTE_WHITE.getCode()) {//TODO: index overflow check
                    x++;//then step on it
                    y++;
                    //if the pixel has lower or right white neighbour, we stop current segment tracing
                    if (extractedLinesArr[y + 1][x] == BYTE_WHITE.getCode()//TODO: index overflow check
                            || extractedLinesArr[y][x + 1] == BYTE_WHITE.getCode()) {
                        break;
                    } else {
                        //otherwise make it black/visited
                        extractedLinesArr[y][x] = BYTE_BLACK.getCode();
                    }
                }
                if (x > xStartOfSegment) {
                    segmentList.add(new PolyLineSegment(new Point(xStartOfSegment - 1, yStartOfSegment - 1), new Point(x - 1,
                            y - 1)));
                }
                //we step on the next pixel
                x = xStartOfSegment + 1;
                y = yStartOfSegment;
            }
        }
        return segmentList;
    }

    private List<PolyLineSegment> traceVerticalPolyLineSegments(byte[][] extractedLinesArr,
            List<PolyLineSegment> segmentList) {
        //for each column
        for (int x = 0; x < extractedLinesArr[0].length; x++) {
            int y = 0;
            //until the end of the column
            while (y < extractedLinesArr.length) {
                //we skip black pixels until the end of the column
                while (y < extractedLinesArr.length && extractedLinesArr[y][x] == BYTE_BLACK.getCode()) {
                    y++;
                }
                //if we are at the end, move on to the next row
                if (y >= extractedLinesArr.length) {
                    break;
                }
                int startOfSegment = y;
                //if the next pixel is white
                boolean endOfLine = true;
                while (extractedLinesArr[y + 1][x] == BYTE_WHITE.getCode()) {//TODO: index overflow check
                    y++;//then step on it
                    //if the pixel has left or right white neighbour, we stop current segment tracing
                    if (extractedLinesArr[y][x - 1] == BYTE_WHITE.getCode()//TODO: index overflow check
                            || extractedLinesArr[y][x + 1] == BYTE_WHITE.getCode()) {
                        endOfLine = false;
                        break;
                    }
                }
                if (y > startOfSegment) {
                    segmentList.add(new PolyLineSegment(new Point(x - 1, startOfSegment - 1), new Point(x - 1, y - 1)));
                }
                //if we ended up at the end of the white line
                if (endOfLine) {
                    y++;//we step on the next black pixel
                }//otherwise we stay on it and therefore make it a new starting point for the next segment
            }
        }
        return segmentList;
    }

    private List<PolyLineSegment> traceHorizontalPolyLineSegments(byte[][] extractedLinesArr,
            List<PolyLineSegment> segmentList) {
        for (int y = 0; y < extractedLinesArr.length; y++) {
            int x = 0;
            //until the end of the row
            while (x < extractedLinesArr[0].length) {
                //we skip black pixels until end of the row
                while (x < extractedLinesArr[0].length && extractedLinesArr[y][x] == BYTE_BLACK.getCode()) {
                    x++;
                }
                //if we are at the end, move on to the next row
                if (x >= extractedLinesArr[0].length) {
                    break;
                }
                int startOfSegment = x;
                //if the next pixel is white
                boolean endOfLine = true;
                while (extractedLinesArr[y][x + 1] == BYTE_WHITE.getCode()) {//TODO: index overflow check
                    x++;//then step on it
                    //if the pixel has upper or lower white neighbour, we stop current segment tracing
                    if (extractedLinesArr[y - 1][x] == BYTE_WHITE.getCode()//TODO: index overflow check
                            || extractedLinesArr[y + 1][x] == BYTE_WHITE.getCode()) {
                        endOfLine = false;
                        break;
                    }
                }
                if (x > startOfSegment) {
                    segmentList.add(new PolyLineSegment(new Point(startOfSegment - 1, y - 1), new Point(x - 1, y - 1)));
                }
                //if we ended up at the end of the white line
                if (endOfLine) {
                    x++;//we step on the next black pixel
                }//otherwise we stay on it and therefore make it a new starting point for the next segment
            }
        }
        return segmentList;
    }

}
