package eu.dindoffer.omr.logic.polyline;

import eu.dindoffer.omr.logic.util.Utils;
import org.opencv.core.Point;

/**
 * A segment of a line. Consists of a left/upper and a right/lower point.
 *
 * @author Martin Dindoffer
 */
public class PolyLineSegment {

    private final Point leftUpperPoint;
    private final Point rightLowerPoint;

    public PolyLineSegment(Point p1, Point p2) {
        if (p1.x < p2.x) {
            leftUpperPoint = p1;
            rightLowerPoint = p2;
        } else if (p2.x < p1.x) {
            leftUpperPoint = p2;
            rightLowerPoint = p1;
        } else if (p1.y < p2.y) {
            leftUpperPoint = p1;
            rightLowerPoint = p2;
        } else if (p2.y < p1.y) {
            leftUpperPoint = p2;
            rightLowerPoint = p1;
        } else {
            leftUpperPoint = p1;
            rightLowerPoint = p2;
        }
    }

    /**
     * Returns the Left point from the segment. If the segment is vertical, returns the upper point.
     *
     * @return the left or upper point
     */
    public Point getLeftUpperPoint() {
        return leftUpperPoint;
    }

    /**
     * Returns the Right point from the segment. If the segment is vertical, returns the lower point.
     *
     * @return the right or lower point
     */
    public Point getRightLowerPoint() {
        return rightLowerPoint;
    }

    public boolean isAttachedTo(PolyLineSegment other) {
        if (Utils.arePointsAdjacent(leftUpperPoint, other.leftUpperPoint) || Utils.arePointsAdjacent(leftUpperPoint,
                other.rightLowerPoint)) {
            return true;
        }
        if (Utils.arePointsAdjacent(rightLowerPoint, other.leftUpperPoint) || Utils.arePointsAdjacent(rightLowerPoint,
                other.rightLowerPoint)) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (int) (leftUpperPoint.x + rightLowerPoint.x + leftUpperPoint.y + rightLowerPoint.y);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PolyLineSegment other = (PolyLineSegment) obj;

        if (!leftUpperPoint.equals(other.leftUpperPoint)) {
            return false;
        }
        if (!rightLowerPoint.equals(other.rightLowerPoint)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "[" + leftUpperPoint + " ; " + rightLowerPoint + "]";
    }
}
