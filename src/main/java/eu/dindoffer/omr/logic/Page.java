package eu.dindoffer.omr.logic;

import static eu.dindoffer.omr.logic.util.ByteColors.BYTE_BLACK;
import static eu.dindoffer.omr.logic.util.ByteColors.BYTE_WHITE;
import static org.opencv.core.Core.REDUCE_SUM;
import static org.opencv.core.Core.reduce;
import static org.opencv.imgproc.Imgproc.threshold;

import eu.dindoffer.omr.logic.notehead.NoteHead;
import eu.dindoffer.omr.logic.notehead.NoteHeadAnnotation;
import eu.dindoffer.omr.logic.notehead.NoteHeadDetector;
import eu.dindoffer.omr.logic.notehead.NoteHeadUnificator;
import eu.dindoffer.omr.logic.polyline.PolyLine;
import eu.dindoffer.omr.logic.polyline.PolyLineSegment;
import eu.dindoffer.omr.logic.polyline.PolyLineTracer;
import eu.dindoffer.omr.logic.skeletonization.ZhangSuenSkeletonizator;
import eu.dindoffer.omr.logic.util.BinaryMat;
import eu.dindoffer.omr.logic.util.Palette;
import eu.dindoffer.omr.logic.util.Utils;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author Martin Dindoffer
 */
public class Page {

    public Operation lastOperation;

    private final Mat original;//1 channel
    private BinaryMat workingMat;//1 channel
    private Mat visualResultMat;
    private Mat verticalHistogram;// histogram of the Page, used for segmentation
    private Mat horizontalHistogram;
    private Mat extractedLines;
    private int staffLineThicknessModus;
    private int staffSpaceHeightModus;
    private int[] blackVerticalLengthHistogram, whiteVerticalLengthHistogram;
    private List<PolyLine> foundPolyLines;
    private List<PolyLine> staffLines;
    private NoteHeadDetector noteHeadDetector;
    private List<NoteHeadAnnotation> noteHeadAnnotations;
    private List<NoteHead> noteHeads;

    public Page(Mat scannedMat) throws IllegalArgumentException {
        if (scannedMat.empty()) {
            throw new IllegalArgumentException("Cannot process empty or nonexisting image.");
        }
        original = scannedMat;
        workingMat = new BinaryMat(original.clone(), true);
        visualResultMat = scannedMat;
        this.lastOperation = Operation.OPEN;
    }

    public NoteHeadDetector getNoteHeadDetector() {
        return noteHeadDetector;
    }

    public void binarizeWithThreshold(int threshold) {
        //toUnsignedInt is a must, because otherwise the conversion to double would produce a real "-1" value, not 255
        threshold(workingMat.getMat(), workingMat.getMat(), threshold, Byte.toUnsignedInt(BYTE_WHITE.getCode()),
                Imgproc.THRESH_BINARY);
        visualResultMat = workingMat.getMat();
    }

    public BinaryMat getCurrentPage() {
        return workingMat;
    }

    public Mat getVisualResultPage() {
        return visualResultMat;
    }

    public int[] getBlackVerticalLengthHistogram() {
        return blackVerticalLengthHistogram;
    }

    public int[] getWhiteVerticalLengthHistogram() {
        return whiteVerticalLengthHistogram;
    }

    public Mat getOriginal() {
        return original;
    }

    public void morphologySmoothing(boolean openingFirst, int openingMaskSize, int closingMaskSize) {
        if (openingFirst) {
            opening(openingMaskSize);
            closing(closingMaskSize);
        } else {
            closing(closingMaskSize);
            opening(openingMaskSize);
        }
    }

    public void opening(int maskSize) {
        Mat mask = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(maskSize, maskSize));
        Imgproc.morphologyEx(workingMat.getMat(), workingMat.getMat(), Imgproc.MORPH_OPEN, mask);
        visualResultMat = workingMat.getMat();
    }

    public void closing(int maskSize) {
        Mat mask = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(maskSize, maskSize));
        Imgproc.morphologyEx(workingMat.getMat(), workingMat.getMat(), Imgproc.MORPH_CLOSE, mask);
        visualResultMat = workingMat.getMat();
    }

    /**
     * Calculates horizontal and vertical projection histograms of current page.
     */
    public void calculateProjectionHistograms() {
        if (!workingMat.hasBlackForeground()) {
            workingMat.invert();
        }
        verticalHistogram = new Mat(workingMat.getMat().rows(), 1, CvType.CV_32S);
        horizontalHistogram = new Mat(1, workingMat.getMat().cols(), CvType.CV_32S);
        reduce(workingMat.getMat(), verticalHistogram, 1, REDUCE_SUM, CvType.CV_32S);
        reduce(workingMat.getMat(), horizontalHistogram, 0, REDUCE_SUM, CvType.CV_32S);
        visualResultMat = drawProjectionsHistograms();
    }

    /**
     * Transforms calculated projection histograms into graphical representations and returns them concatenated with current
     * page. Current page remains unchanged.
     *
     * @return current page with added histograms
     */
    private Mat drawProjectionsHistograms() {
        int[] buffVH = new int[(int) verticalHistogram.total() * verticalHistogram.channels()];//this is specially for 32S
        verticalHistogram.get(0, 0, buffVH);//move data to the buffer
        int max = 0;//find maximum
        int min = Integer.MAX_VALUE;//and minimum
        for (int i = 0; i < buffVH.length; i++) {
            if (buffVH[i] > max) {
                max = buffVH[i];
            } else if (buffVH[i] < min) {
                min = buffVH[i];
            }
        }
        Mat canvasForVH = Mat.zeros(workingMat.getMat().rows(), 100, CvType.CV_8UC1);
        int rownum = 0;
        for (int i = 0; i < buffVH.length; i++) {
            Imgproc.line(canvasForVH, new Point(0, rownum), new Point((max - buffVH[i]) * 100.0 / (max - min), rownum),
                    new Scalar(255));
            rownum++;
        }

        int[] buffHH = new int[(int) horizontalHistogram.total() * horizontalHistogram.channels()];
        horizontalHistogram.get(0, 0, buffHH);//move data to the buffer
        max = 0;//find maximum
        min = Integer.MAX_VALUE;//and minimum
        for (int i = 0; i < buffHH.length; i++) {
            if (buffHH[i] > max) {
                max = buffHH[i];
            } else if (buffHH[i] < min) {
                min = buffHH[i];
            }
        }
        Mat canvasForHH = Mat.zeros(100, workingMat.getMat().cols() + canvasForVH.cols(), CvType.CV_8UC1);
        int colnum = canvasForVH.cols();
        for (int i = 0; i < buffHH.length; i++) {
            Imgproc.line(canvasForHH, new Point(colnum, 0), new Point(colnum, (max - buffHH[i]) * 100.0 / (max - min)),
                    new Scalar(255));
            colnum++;
        }

        List<Mat> lh = new LinkedList<>();
        lh.add(canvasForVH);
        lh.add(workingMat.getMat());
        Mat result = new Mat(workingMat.getMat().rows(), workingMat.getMat().cols() + canvasForVH.cols(), CvType.CV_8UC1);
        Core.hconcat(lh, result);

        List<Mat> lv = new LinkedList<>();
        lv.add(canvasForHH);
        lv.add(result);
        Mat result2 = new Mat(workingMat.getMat().rows() + canvasForHH.rows(), result.cols(), CvType.CV_8UC1);
        Core.vconcat(lv, result2);

        return result2;
    }

    /**
     * Calculates histograms of lengths of continuous vertical runs in the current page. As a side product a modus of staff
     * space height is calculated.
     *
     * @param maxLengthToTrack maximum length in pixels to track
     */
    public void calcVerticalLengthHistograms(int maxLengthToTrack) {
        if (!workingMat.hasBlackForeground()) {
            workingMat.invert();
        }
        //load Mat into java array
        //loaded bytes are unsigned, and should be treated that way!!
        //Byte.toUnsignedInt() is not needed, cause only 2 values are present (0/-1)
        byte buff[] = new byte[(int) workingMat.getMat().total()];
        workingMat.getMat().get(0, 0, buff); // returned by rows
        int[] blacks = new int[maxLengthToTrack + 1];
        int[] whites = new int[maxLengthToTrack + 1];

        byte curPix = buff[0];
        int imgWidth = workingMat.getMat().width();
        int imgHeight = workingMat.getMat().height();
        int count = 0;
        int curColumn = 0;
        for (int index = 0; index < workingMat.getMat().total();) {
            if (buff[index] == curPix) {
                count++;
            } else {
                if (count <= maxLengthToTrack) {
                    if (curPix == BYTE_BLACK.getCode()) {
                        blacks[count]++;
                    } else {
                        whites[count]++;
                    }
                }
                curPix = buff[index];
                count = 1;
            }
            index += imgWidth;//move on to the next row in the same column
            if (index > imgHeight * (curColumn + 1)) {
                curColumn++;
                index = curColumn;
                count = 0;
                curPix = buff[index];
            }
        }

        int temp = 0;
        for (int i = 0; i < blacks.length; i++) {
            if (blacks[i] > blacks[temp]) {
                temp = i;
            }
        }
        staffLineThicknessModus = temp;
        temp = 0;
        for (int i = 0; i < whites.length; i++) {
            if (whites[i] > whites[temp]) {
                temp = i;
            }
        }
        staffSpaceHeightModus = temp;
        blackVerticalLengthHistogram = blacks;
        whiteVerticalLengthHistogram = whites;
    }

    /**
     * Performs a probabilistic Hough algorithm on the current page to extract horizontal line segments.
     */
    public void probabilisticHough() {
        if (workingMat.hasBlackForeground()) {
            workingMat.invert();
        }
        Mat lines = new Mat();

        Imgproc.HoughLinesP(workingMat.getMat(), lines, Settings.rho, (Math.PI / 180) * Settings.theta,
                Settings.houghThreshold, Settings.minLineSegmentLength, Settings.maxLineSegmentGap);
        int[] vectors = new int[(int) lines.total() * lines.channels()];
        lines.get(0, 0, vectors);
        Mat colorMat = new Mat(workingMat.getMat().rows(), workingMat.getMat().cols(), CvType.CV_8UC3);
        Mat staffLinesMat = Mat.zeros(workingMat.getMat().rows(), workingMat.getMat().cols(), CvType.CV_8UC1);
        Imgproc.cvtColor(workingMat.getMat(), colorMat, Imgproc.COLOR_GRAY2BGR);
        Scalar foregroundColor = new Scalar(255);
        int counter = 0;
        for (int i = 0; i < lines.rows(); i += 4) {
            int x1 = vectors[i];
            int y1 = vectors[i + 1];
            int x2 = vectors[i + 2];
            int y2 = vectors[i + 3];

            double lineSegmentAngle = 90;
            //x1 is never on the right side thanks to opencv implementation
            double horizontalDistance = x2 - x1;
            if (horizontalDistance > 0) {
                if (y1 <= y2) {//if point 2 is lower
                    lineSegmentAngle = Math.toDegrees(Math.atan((y2 - y1) / (horizontalDistance)));
                } else {
                    lineSegmentAngle = Math.toDegrees(Math.atan((y1 - y2) / (horizontalDistance)));
                }
            }

            //only segments forming less than maximum allowed degree angle in relation to X axis
            if (lineSegmentAngle < Settings.maxLineSegmentAngleRotation) {
                counter++;
                Point firstPoint = new Point(x1, y1);
                Point secondPoint = new Point(x2, y2);
                Imgproc.line(colorMat, firstPoint, secondPoint, Palette.getRandomBrightColor(), 2);
                Imgproc.line(staffLinesMat, firstPoint, secondPoint, foregroundColor, 2);
            }
        }
        System.out.println("Number of segments detected by Hough with angle smaller than "
                + Settings.maxLineSegmentAngleRotation + "deg: " + counter);
        visualResultMat = colorMat;
        extractedLines = staffLinesMat;
    }

    /**
     * Skeletonizes the extracted horizontal line segments using Zhang-Suen algorithm.
     */
    public void skeletonizeExtractedLines() {
        ZhangSuenSkeletonizator zhs = new ZhangSuenSkeletonizator();
        zhs.skeletonize(extractedLines);
        visualResultMat = extractedLines;
    }

    /**
     * Finds the polylines in the skeletonized image by manual tracing of the line segments and finding the shortest (in
     * terms of the number of line segments) horizontal path using Dijkstra.
     */
    public void findPolyLines() {
        PolyLineTracer tracer = new PolyLineTracer();
        List<PolyLineSegment> polyLinesList = tracer.tracePolyLineSegments(extractedLines);
        Mat tracedLines = Mat.zeros(extractedLines.size(), CvType.CV_8UC3);
        foundPolyLines = new LinkedList<>();
        while (polyLinesList.isEmpty() == false) {
            Iterator<PolyLineSegment> i = polyLinesList.iterator();
            PolyLineSegment pls = i.next();
            i.remove();
            List<PolyLineSegment> attachedSegments = new LinkedList<>();
            attachedSegments.add(pls);

            while (i.hasNext()) {
                PolyLineSegment potentialMatch = i.next();
                for (PolyLineSegment segment : attachedSegments) {
                    if (potentialMatch.isAttachedTo(segment)) {
                        attachedSegments.add(potentialMatch);
                        i.remove();
                        i = polyLinesList.iterator();
                        break;
                    }
                }
            }

            PolyLine polyLine = new PolyLine(attachedSegments);
            //polyLine.comb();
            polyLine.combByDijkstra();
            foundPolyLines.add(polyLine);
            Scalar pathColor = Palette.getRandomBrightColor();
            for (PolyLineSegment drawableSegment : polyLine.getPolyLineSegments()) {
                Imgproc.line(tracedLines, drawableSegment.getLeftUpperPoint(), drawableSegment.getRightLowerPoint(),
                        pathColor);
            }
        }
        visualResultMat = tracedLines;
    }

    /**
     * Detects the Staff lines among the found polylines. Fills in missing left and right segments to fit the guessed
     * staffline length.
     */
    public void findStaffLines() {
        //first identify the long polylines, drop others
        staffLines = new LinkedList<>();
        for (PolyLine pl : foundPolyLines) {
            if (pl.calculateWidth() > (0.5 * workingMat.getMat().width())) {
                staffLines.add(pl);
            }
        }
        //now find the 10 utmost points
        List<Point> leftmostPoints = new LinkedList<>();
        List<Point> rightmostPoints = new LinkedList<>();
        for (PolyLine pl : staffLines) {
            PolyLineSegment[] segments = pl.findLeftAndRightEndPointSegments();
            leftmostPoints.add(segments[0].getLeftUpperPoint());
            rightmostPoints.add(segments[1].getRightLowerPoint());
        }
        leftmostPoints.sort((Point p1, Point p2) -> (int) (p1.x - p2.x));
        rightmostPoints.sort((Point p1, Point p2) -> (int) (p2.x - p1.x));

        //now calculate the arithmetic mean of the top 5 candidates
        int leftEdge, rightEdge;
        int sumLeft = 0;
        int sumRight = 0;
        for (int i = 0; i < 5; i++) {
            sumLeft += leftmostPoints.get(i).x;
            sumRight += rightmostPoints.get(i).x;
        }
        leftEdge = sumLeft / 5;
        rightEdge = sumRight / 5;
        System.out.println("Left edge: " + leftEdge);
        System.out.println("Right edge: " + rightEdge);

        //now sort the Lines by their avg Y
        for (PolyLine pl : staffLines) {
            pl.calcAvgY();
        }
        staffLines.sort((PolyLine pl1, PolyLine pl2) -> pl1.getAvgY() - pl2.getAvgY());

        //copy the translated segments into the shorter lines if applicable
        for (int i = 0; i < staffLines.size(); i++) {
            PolyLine currentPolyLine = staffLines.get(i);
            PolyLineSegment[] endSegments = currentPolyLine.findLeftAndRightEndPointSegments();
            Point leftUpperEnd = endSegments[0].getLeftUpperPoint();
            Point rightLowerEnd = endSegments[1].getRightLowerPoint();
            //find the closes line
            PolyLine closestPolyLine;
            if (i == 0) {
                closestPolyLine = staffLines.get(i + 1);
            } else if (i == (staffLines.size() - 1)) {
                closestPolyLine = staffLines.get(i - 1);
            } else {
                PolyLine prev = staffLines.get(i - 1);
                PolyLine next = staffLines.get(i + 1);
                //if the previous is closer
                if ((currentPolyLine.getAvgY() - prev.getAvgY()) < (next.getAvgY() - currentPolyLine.getAvgY())) {
                    closestPolyLine = prev;
                } else {
                    closestPolyLine = next;
                }
            }
            int yDiff = currentPolyLine.getAvgY() - closestPolyLine.getAvgY();
            // round the diff to a nearest multiple of the staffHeightSpace modus + staffLineThicknessModus
            int roundToValue = staffSpaceHeightModus + staffLineThicknessModus;
            yDiff = (int) (roundToValue * (Math.round(yDiff / (double) roundToValue)));

            //in other words - dont do the segment copying if the closest line is obviously on a different stave
            if (yDiff < 5 * roundToValue) {
                //duplicate the segments of the closest line
                for (PolyLineSegment closestSegment : closestPolyLine.getPolyLineSegments()) {
                    //for the left side
                    if (leftUpperEnd.x > closestSegment.getLeftUpperPoint().x) {
                        //if we found a segment that should be copied
                        if (closestSegment.getRightLowerPoint().x < leftUpperEnd.x) {
                            // if the segment does not horizontally overlap, we can simply add it (translated ofc)
                            Point luPoint = new Point(closestSegment.getLeftUpperPoint().x,
                                    closestSegment.getLeftUpperPoint().y + yDiff);
                            Point rlPoint = new Point(closestSegment.getRightLowerPoint().x,
                                    closestSegment.getRightLowerPoint().y + yDiff);
                            PolyLineSegment segmentToAdd = new PolyLineSegment(luPoint, rlPoint);
                            currentPolyLine.getPolyLineSegments().add(segmentToAdd);
                        } else {
                            //if the segment overlaps horizontally, we make a new one, connecting the rest of the segments
                            Point luPoint = new Point(closestSegment.getLeftUpperPoint().x,
                                    closestSegment.getLeftUpperPoint().y + yDiff);
                            Point rlPoint = leftUpperEnd.clone();
                            PolyLineSegment segmentToAdd = new PolyLineSegment(luPoint, rlPoint);
                            currentPolyLine.getPolyLineSegments().add(segmentToAdd);
                        }
                    }

                    //now the same for the right side
                    if (rightLowerEnd.x < closestSegment.getRightLowerPoint().x) {
                        //if we found a segment that should be copied
                        if (closestSegment.getLeftUpperPoint().x > rightLowerEnd.x) {
                            // if the segment does not horizontally overlap, we can simply add it (translated ofc)
                            Point luPoint = new Point(closestSegment.getLeftUpperPoint().x,
                                    closestSegment.getLeftUpperPoint().y + yDiff);
                            Point rlPoint = new Point(closestSegment.getRightLowerPoint().x,
                                    closestSegment.getRightLowerPoint().y + yDiff);
                            PolyLineSegment segmentToAdd = new PolyLineSegment(luPoint, rlPoint);
                            currentPolyLine.getPolyLineSegments().add(segmentToAdd);
                        } else {
                            //if the segment overlaps horizontally, we make a new one, connecting the rest of the segments
                            Point rlPoint = new Point(closestSegment.getRightLowerPoint().x,
                                    closestSegment.getRightLowerPoint().y + yDiff);
                            Point luPoint = rightLowerEnd.clone();
                            PolyLineSegment segmentToAdd = new PolyLineSegment(luPoint, rlPoint);
                            currentPolyLine.getPolyLineSegments().add(segmentToAdd);
                        }
                    }
                }
            }
        }

        //now we fill the remaining width with simple horizontal segments
        for (PolyLine currentPolyLine : staffLines) {
            //now reset the end points of the current line
            PolyLineSegment[] endSegments = currentPolyLine.findLeftAndRightEndPointSegments();
            Point leftUpperEnd = endSegments[0].getLeftUpperPoint();
            Point rightLowerEnd = endSegments[1].getRightLowerPoint();
            //and fill the remaining simple horizontal segment
            if (leftUpperEnd.x > leftEdge) {
                currentPolyLine.getPolyLineSegments().add(new PolyLineSegment(new Point(leftEdge, leftUpperEnd.y),
                        leftUpperEnd.clone()));
            }
            if (rightLowerEnd.x < rightEdge) {
                currentPolyLine.getPolyLineSegments().add(
                        new PolyLineSegment(rightLowerEnd.clone(), new Point(rightEdge, rightLowerEnd.y)));
            }
        }

        Mat colorMat = new Mat(workingMat.getMat().rows(), workingMat.getMat().cols(), CvType.CV_8UC3);
        Imgproc.cvtColor(workingMat.getMat(), colorMat, Imgproc.COLOR_GRAY2BGR);
        for (PolyLine staffLine : staffLines) {
            Scalar staffLineColor = Palette.getRandomBrightColor();
            for (PolyLineSegment pls : staffLine.getPolyLineSegments()) {
                Imgproc.line(colorMat, pls.getLeftUpperPoint(), pls.getRightLowerPoint(), staffLineColor);
            }
        }
        visualResultMat = colorMat;
    }

    /**
     * Removes the detected staff lines from the working copy of the page
     */
    public void removeStaffLines() {
        if (workingMat.hasBlackForeground()) {
            workingMat.invert();
        }
        //workaround for missing LineIterator in OCV Java bindings is to draw the lines and find nonzero pixels
        Mat drawnLinesMat = Mat.zeros(workingMat.getMat().size(), CvType.CV_8UC1);
        for (PolyLine staffLine : staffLines) {
            for (PolyLineSegment staffLineSegment : staffLine.getPolyLineSegments()) {
                Imgproc.line(drawnLinesMat, staffLineSegment.getLeftUpperPoint(), staffLineSegment.getRightLowerPoint(),
                        new Scalar(255));
            }
        }
        Mat linesCoordinatesMat = new Mat();
        Core.findNonZero(drawnLinesMat, linesCoordinatesMat);
        MatOfPoint pixelsMat = new MatOfPoint(linesCoordinatesMat);
        List<Point> pixels = pixelsMat.toList();

        //now to remove the lines
        byte[][] page = Utils.mat8UC1ToByteArrayYX(workingMat.getMat());
        for (Point p : pixels) {
            int lineX = (int) p.x;
            int lineY = (int) p.y;
            if (page[lineY][lineX] == BYTE_WHITE.getCode()) {//if we are on a foreground
                int fgCounter = 0;
                int shift = 1;
                int topBorder = lineY;
                while (page[lineY - shift][lineX] == BYTE_WHITE.getCode()) {
                    fgCounter++;
                    topBorder = lineY - shift;
                    shift++;
                }
                //if we are on a pixel that is a part of a bigger object we leave it be
                if (fgCounter > (staffLineThicknessModus * 1.5)) {
                    continue;
                }
                //now the same downwards
                int bottomBorder = lineY;
                shift = 0;
                while (page[lineY + shift][lineX] == BYTE_WHITE.getCode()) {
                    fgCounter++;
                    bottomBorder = lineY + shift;
                    shift++;
                }
                if (fgCounter > (staffLineThicknessModus * 1.5)) {
                    continue;
                }
                //if we are on a small-height object, we remove the column
                for (int row = topBorder; row <= bottomBorder; row++) {
                    page[row][lineX] = BYTE_BLACK.getCode();
                }
            }
        }
        //now push the Mat back
        for (int row = 0; row < workingMat.getMat().rows(); row++) {
            workingMat.getMat().put(row, 0, page[row]);
        }
        visualResultMat = workingMat.getMat();
    }

    public List<NoteHeadAnnotation> detectNoteHeads(boolean drawResults, boolean originalScaleAnnotations) {
        this.noteHeadDetector = new NoteHeadDetector(staffSpaceHeightModus);
        this.noteHeadAnnotations = noteHeadDetector.detectNoteHeads(workingMat, true);
        if (drawResults) {
            drawAnnotations(noteHeadAnnotations);
        }
        return noteHeadAnnotations;
    }

    public void unifyNoteHeadAnnotations() {
        noteHeads = new NoteHeadUnificator().unifyNoteHeadAnnotations(noteHeadAnnotations);
        drawNoteHeads();
    }

    public void resolveMultipleNoteHeads() {
        noteHeads = new NoteHeadUnificator().unifyNoteHeads(noteHeads, false);
        drawNoteHeads();
    }

    private void drawNoteHeads() {
        Imgproc.cvtColor(workingMat.getMat(), visualResultMat, Imgproc.COLOR_GRAY2BGR);
        for (NoteHead nh : noteHeads) {
            Point[] points = nh.getBbox().getOpenCVDefiningPoints();
            Imgproc.rectangle(visualResultMat, points[0], points[1], nh.getType().getBboxColor());
        }
    }

    private void drawAnnotations(List<NoteHeadAnnotation> annotations) {
        Mat newVisualisation = new Mat();
        Imgproc.cvtColor(workingMat.getMat(), newVisualisation, Imgproc.COLOR_GRAY2BGR);
        for (NoteHeadAnnotation annotation : annotations) {
            Imgproc.rectangle(newVisualisation, annotation.getTopLeftHighDPI(), annotation.getBottomRightHighDPI(),
                    annotation.getType().getBboxColor());
        }
        visualResultMat = newVisualisation;
    }
}
