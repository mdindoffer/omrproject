package eu.dindoffer.omr.logic.util;

import java.awt.Color;
import java.util.Random;
import org.opencv.core.Scalar;

/**
 *
 * @author Martin Dindoffer
 */
public class Palette {

    private static final Random RANDOMIZER = new Random();
    private static final float RAND_COLOR_SATURATION = 0.9f;//1.0 for brilliant, 0.0 for dull
    private static final float RAND_COLOR_LUMINANCE = 1.0f; //1.0 for brighter, 0.0 for black

    /**
     * Generates random saturated and bright color using the HSB color mobel.
     *
     * @return random color
     */
    public static Scalar getRandomBrightColor() {
        float hue = RANDOMIZER.nextFloat();
        Color color = Color.getHSBColor(hue, RAND_COLOR_SATURATION, RAND_COLOR_LUMINANCE);
        return new Scalar(color.getBlue(), color.getGreen(), color.getRed());
    }
}
