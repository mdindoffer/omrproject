package eu.dindoffer.omr.logic.util;

import org.opencv.core.Mat;

/**
 *
 * @author Martin Dindoffer
 */
public class SVMTuplet {

    private final Mat samplesMat;
    private final Mat labelsMat;

    public SVMTuplet(Mat trainingMat, Mat labelsMat) {
        this.samplesMat = trainingMat;
        this.labelsMat = labelsMat;
    }

    public Mat getSamplesMat() {
        return samplesMat;
    }

    public Mat getLabelsMat() {
        return labelsMat;
    }

}
