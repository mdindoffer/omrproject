package eu.dindoffer.omr.logic.util;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import org.opencv.core.Mat;
import org.opencv.core.Point;

/**
 * Various utility methods.
 *
 * @author Martin Dindoffer
 */
public class Utils {

    /**
     * Converts either 1 or 3 channelled (BGR) 8 bit unsigned Mat image to BufferedImage.
     *
     * @param m 1 or 3 channelled (BGR) 8 bit unsigned Mat
     * @return converted BufferedImage
     */
    public static BufferedImage toBufferedImage(Mat m) {
        int type = BufferedImage.TYPE_BYTE_GRAY;
        if (m.channels() > 1) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = m.channels() * m.cols() * m.rows();
        byte[] b = new byte[bufferSize];
        m.get(0, 0, b); // get all the pixels
        BufferedImage image = new BufferedImage(m.cols(), m.rows(), type);
        byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);
        return image;
    }

    /**
     * Converts 8UC1 Mat into a 2-dimensional byte array with [Y][X] xoordinates.
     *
     * @param mat 8UC1 Mat
     * @return converted byte array
     */
    public static byte[][] mat8UC1ToByteArrayYX(Mat mat) {
        byte[][] arr = new byte[mat.rows()][mat.cols()];
        for (int rowIndex = 0; rowIndex < mat.rows(); rowIndex++) {
            mat.get(rowIndex, 0, arr[rowIndex]);
        }
        return arr;
    }

    /**
     * Tests if two given points are within 1 pixel (inclusive) apart.
     *
     * @param p1 first point
     * @param p2 second point
     * @return true if points are closer than 1 pixel apart (inclusive), false otherwise
     */
    public static boolean arePointsAdjacent(Point p1, Point p2) {
        if (Math.abs(p1.x - p2.x) <= 1) {
            if (Math.abs(p1.y - p2.y) <= 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * Scales a given point to a higher DPI coordinates.
     *
     * @param p point to scale
     * @param scalingFactor scaling factor to use (0;1)
     */
    public static void scalePointUp(Point p, double scalingFactor) {
        p.x = p.x / scalingFactor;
        p.y = p.y / scalingFactor;
    }

    /**
     * Scales a given point to a smaller DPI coordinates.
     *
     * @param p point to scale
     * @param scalingFactor scaling factor to use (0;1)
     */
    public static void scalePointDown(Point p, double scalingFactor) {
        p.x = p.x * scalingFactor;
        p.y = p.y * scalingFactor;
    }
}
