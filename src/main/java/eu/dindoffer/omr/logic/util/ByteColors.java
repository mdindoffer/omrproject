package eu.dindoffer.omr.logic.util;

/**
 * Black and white colors as byte values.
 *
 * @author Martin Dindoffer
 */
public enum ByteColors {
    BYTE_WHITE((byte) 255), BYTE_BLACK((byte) 0);
    private final byte code;

    private ByteColors(byte code) {
        this.code = code;
    }

    public byte getCode() {
        return code;
    }

}
