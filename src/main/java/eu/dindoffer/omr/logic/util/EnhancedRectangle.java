package eu.dindoffer.omr.logic.util;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

/**
 *
 * @author Martin Dindoffer
 */
public class EnhancedRectangle extends Rectangle {

    public EnhancedRectangle() {
    }

    public EnhancedRectangle(Rectangle r) {
        super(r);
    }

    public EnhancedRectangle(int x, int y, int width, int height) {
        super(x, y, width, height);
    }

    public EnhancedRectangle(int width, int height) {
        super(width, height);
    }

    public EnhancedRectangle(Point p, Dimension d) {
        super(p, d);
    }

    public EnhancedRectangle(Point p) {
        super(p);
    }

    public EnhancedRectangle(Dimension d) {
        super(d);
    }

    public double calculateSurface() {
        return this.width * this.height;
    }

    public EnhancedRectangle intersection(EnhancedRectangle r) {
        return new EnhancedRectangle(super.intersection(r));
    }

    public EnhancedRectangle union(EnhancedRectangle r) {
        return new EnhancedRectangle(super.union(r));
    }

    public double intersectionOverUnion(EnhancedRectangle r) {
        if (this.intersects(r)) {
            return this.intersection(r).calculateSurface() / this.union(r).calculateSurface();
        } else {
            return 0;
        }
    }

    public org.opencv.core.Point[] getOpenCVDefiningPoints() {
        org.opencv.core.Point[] points = new org.opencv.core.Point[2];
        points[0] = new org.opencv.core.Point(x, y);
        points[1] = new org.opencv.core.Point(x + width, y + height);
        return points;
    }
}
