package eu.dindoffer.omr.logic.util;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * Simple wrapper for a binary 8UC1 Mat
 *
 * @author Martin Dindoffer
 */
public class BinaryMat {

    private final Mat mat;
    private boolean blackForeground;

    public BinaryMat(Mat mat, boolean blackForeground) {
        if (mat.type() != CvType.CV_8UC1) {
            throw new IllegalArgumentException("Not a 8UC1 mat");
        }
        this.mat = mat;
        this.blackForeground = blackForeground;
    }

    public Mat getMat() {
        return mat;
    }

    public boolean hasBlackForeground() {
        return blackForeground;
    }

    public void invert() {
        Mat subtractHelper = new Mat(mat.rows(), mat.cols(), mat.type(), new Scalar(255));
        Core.subtract(subtractHelper, mat, mat);
        blackForeground = !blackForeground;
    }

    public BinaryMat createResizedCopy(double scalingFactor) {
        Mat resizedMat = new Mat();
        Imgproc.resize(mat, resizedMat, new Size(), scalingFactor, scalingFactor, Imgproc.INTER_AREA);
        return new BinaryMat(resizedMat, blackForeground);
    }
}
