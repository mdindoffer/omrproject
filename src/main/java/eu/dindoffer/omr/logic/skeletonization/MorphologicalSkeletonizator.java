package eu.dindoffer.omr.logic.skeletonization;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * http://felix.abecassis.me/2011/09/opencv-morphological-skeleton/
 *
 * @author Martin Dindoffer
 */
public class MorphologicalSkeletonizator {

    public void skeletonize(Mat mat) throws IllegalArgumentException {
        if (mat.type() != CvType.CV_8UC1) {
            throw new IllegalArgumentException("Provided Mat is not 8UC1");
        }
        Mat skel = new Mat(mat.size(), CvType.CV_8UC1, new Scalar(0));
        Mat temp = new Mat();
        Mat eroded = new Mat();
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_CROSS, new Size(3, 3));
        boolean done;

        do {
            Imgproc.erode(mat, eroded, element);
            Imgproc.dilate(eroded, temp, element);
            Core.subtract(mat, temp, temp);
            Core.bitwise_or(skel, temp, skel);
            eroded.copyTo(mat);
            done = (Core.countNonZero(mat) == 0);
        } while (!done);
        mat = skel;
    }
}
