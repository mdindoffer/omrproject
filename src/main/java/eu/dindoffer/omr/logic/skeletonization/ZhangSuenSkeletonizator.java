package eu.dindoffer.omr.logic.skeletonization;

import static eu.dindoffer.omr.logic.util.ByteColors.BYTE_BLACK;
import static eu.dindoffer.omr.logic.util.Utils.mat8UC1ToByteArrayYX;
import java.util.LinkedList;
import java.util.List;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

/**
 *
 * @author Martin Dindoffer
 */
public class ZhangSuenSkeletonizator {

    /**
     * Skeletonize given Mat
     *
     * @param mat white foreground, thresholded image, 8UC1
     * @throws IllegalArgumentException if mat is not 8UC1
     */
    public void skeletonize(Mat mat) throws IllegalArgumentException {
        if (mat.type() != CvType.CV_8UC1) {
            throw new IllegalArgumentException("Provided Mat is not 8UC1");
        }

        byte[][] image = mat8UC1ToByteArrayYX(mat);

        List<PointMarker> points1;
        List<PointMarker> points2;
        while (true) {
            points1 = zhangSuenIteration(image, true);
            for (PointMarker point : points1) {
                image[point.x][point.y] = BYTE_BLACK.getCode();
            }
            points2 = zhangSuenIteration(image, false);
            for (PointMarker point : points2) {
                image[point.x][point.y] = BYTE_BLACK.getCode();
            }
            if (points1.isEmpty() && points2.isEmpty()) {
                break;
            }
        }

        for (int row = 0; row < mat.rows(); row++) {
            mat.put(row, 0, image[row]);
        }
    }

    private List<PointMarker> zhangSuenIteration(byte[][] image, boolean oddIteration) {
        LinkedList<PointMarker> pointsToChange = new LinkedList<>();
        for (int row = 1; row < image.length - 2; row++) {
            for (int col = 1; col < image[row].length - 2; col++) {
                if (image[row][col] != BYTE_BLACK.getCode()) {
                    byte neighbourN = image[row - 1][col];
                    byte neighbourNE = image[row - 1][col + 1];
                    byte neighbourE = image[row][col + 1];
                    byte neighbourSE = image[row + 1][col + 1];
                    byte neighbourS = image[row + 1][col];
                    byte neighbourSW = image[row + 1][col - 1];
                    byte neighbourW = image[row][col - 1];
                    byte neighbourNW = image[row - 1][col - 1];
                    byte[] neighboursArray = new byte[8];
                    neighboursArray[0] = neighbourN;
                    neighboursArray[1] = neighbourNE;
                    neighboursArray[2] = neighbourE;
                    neighboursArray[3] = neighbourSE;
                    neighboursArray[4] = neighbourS;
                    neighboursArray[5] = neighbourSW;
                    neighboursArray[6] = neighbourW;
                    neighboursArray[7] = neighbourNW;

                    int sum = 0;
                    for (Byte b : neighboursArray) {
                        sum += b;
                    }
                    sum = Math.abs(sum);

                    if (sum >= 2 && sum <= 6) {
                        int numOfTransitions = 0;
                        for (int i = 0; i < neighboursArray.length; i++) {
                            if (neighboursArray[i] == BYTE_BLACK.getCode() && neighboursArray[(i + 1) % 8] == -1) {
                                numOfTransitions++;
                            }
                        }

                        if (numOfTransitions == 1) {
                            if (oddIteration) {
                                if (neighbourN * neighbourE * neighbourS == 0 && neighbourE * neighbourS * neighbourW == 0) {
                                    pointsToChange.add(new PointMarker(row, col));
                                }
                            } else {
                                if (neighbourN * neighbourE * neighbourW == 0 && neighbourN * neighbourS * neighbourW == 0) {
                                    pointsToChange.add(new PointMarker(row, col));
                                }
                            }
                        }
                    }
                }
            }
        }
        return pointsToChange;
    }

    private static class PointMarker {

        public final int x, y;

        public PointMarker(int x, int y) {
            this.x = x;
            this.y = y;
        }

    }
}
