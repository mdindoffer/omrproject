package eu.dindoffer.omr.logic;

import java.util.LinkedList;
import java.util.List;

/**
 * Algorithm steps.
 *
 * @author Martin Dindoffer
 */
public enum Operation {
    OPEN(null), BINARIZE(OPEN), DENOISE(BINARIZE), PROJECT_HISTOGRAMS(BINARIZE), CALCULATE_VERTICAL_LENGTHS(BINARIZE),
    HOUGH_TRANSFORM(CALCULATE_VERTICAL_LENGTHS), SKELETONIZE(HOUGH_TRANSFORM), FIND_POLYLINES(SKELETONIZE),
    FIND_STAFF_LINES(FIND_POLYLINES),  DETECT_NOTE_HEADS(CALCULATE_VERTICAL_LENGTHS),
    UNIFY_NH_ANNOTATIONS(DETECT_NOTE_HEADS), RESOLVE_NH_MULTIPLICITIES(UNIFY_NH_ANNOTATIONS),
    REMOVE_STAFF_LINES(FIND_STAFF_LINES);

    /**
     * Operation that is needed to complete before this one.
     */
    private final Operation dependentOperation;

    public static final List<Operation> PIPELINE_ORDERING = generatePipeline();

    /**
     * Specifies the order of the algorithm steps in the recognition pipeline.
     *
     * @return pipeline of ordered operations
     */
    private static List<Operation> generatePipeline() {
        List<Operation> pipeline = new LinkedList<>();
        pipeline.add(OPEN);
        pipeline.add(BINARIZE);
        pipeline.add(DENOISE);
        pipeline.add(PROJECT_HISTOGRAMS);
        pipeline.add(CALCULATE_VERTICAL_LENGTHS);
        pipeline.add(HOUGH_TRANSFORM);
        pipeline.add(SKELETONIZE);
        pipeline.add(FIND_POLYLINES);
        pipeline.add(FIND_STAFF_LINES);
        pipeline.add(DETECT_NOTE_HEADS);
        pipeline.add(UNIFY_NH_ANNOTATIONS);
        pipeline.add(RESOLVE_NH_MULTIPLICITIES);
        pipeline.add(REMOVE_STAFF_LINES);
        return pipeline;
    }

    private Operation(Operation dependentOperation) {
        this.dependentOperation = dependentOperation;
    }

    public Operation getDependentOperation() {
        return dependentOperation;
    }

    /**
     * Decide whether this algorithm step can be executed directly after the given one.
     *
     * @param baseOperation starting (base) operation
     * @return true if the execution can be made, false otherwise
     */
    public boolean canExecuteAfter(Operation baseOperation) {
        if (PIPELINE_ORDERING.indexOf(this) <= PIPELINE_ORDERING.indexOf(baseOperation)) {
            return false; //can't move backwards in pipeline
        }
        if (PIPELINE_ORDERING.indexOf(baseOperation) < PIPELINE_ORDERING.indexOf(this.dependentOperation)) {
            return false; //can't skip needed operations
        }
        return true;
    }

    /**
     * Decide whether the given algorithm step / operation can be executed right after this one.
     *
     * @param desiredOperation algorithm step to move to
     * @return true if the execution can be made, false otherwise
     */
    public boolean canExecutionFollowTo(Operation desiredOperation) {
        return desiredOperation.canExecuteAfter(this);
    }
}
