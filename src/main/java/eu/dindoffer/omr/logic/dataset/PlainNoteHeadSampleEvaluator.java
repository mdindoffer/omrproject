package eu.dindoffer.omr.logic.dataset;

import com.github.habernal.confusionmatrix.ConfusionMatrix;
import eu.dindoffer.omr.logic.notehead.NoteHeadSVM;
import eu.dindoffer.omr.logic.notehead.NoteHeadType;
import eu.dindoffer.omr.logic.util.SVMTuplet;
import java.io.File;
import java.io.IOException;
import org.opencv.core.Mat;

/**
 * Evaluates the plain note head SVM model on pre-cut samples.
 *
 * @author Martin Dindoffer
 */
public class PlainNoteHeadSampleEvaluator {

    public String evaluate(File noteHeadDir) throws IOException {
        NoteHeadSVM svm = NoteHeadSVM.getInstance();
        svm.reloadModel();
        SVMTuplet loadedSamples = svm.loadNoteHeadSamples(noteHeadDir);
        Mat results = svm.predictNoteHeads(loadedSamples.getSamplesMat());
        ConfusionMatrix cm = new ConfusionMatrix();
        for (int i = 0; i < loadedSamples.getLabelsMat().height(); i++) {
            NoteHeadType expectedType = parseNoteHeadType(loadedSamples.getLabelsMat(), i);
            NoteHeadType observedType = parseNoteHeadType(results, i);
            cm.increaseValue(expectedType.name(), observedType.name());
        }
        System.out.println(cm);
        return cm.toString();
    }

    private NoteHeadType parseNoteHeadType(Mat classLabels, int sampleNum) {
        float[] data = new float[1];
        classLabels.get(sampleNum, 0, data);
        return NoteHeadType.getById((int) data[0]);
    }
}
