package eu.dindoffer.omr.logic.dataset;

import eu.dindoffer.omr.logic.notehead.NoteHeadAnnotation;
import eu.dindoffer.omr.logic.notehead.NoteHeadType;
import eu.dindoffer.omr.logic.util.EnhancedRectangle;
import java.io.File;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Bullshit
 *
 * @author Martin Dindoffer
 */
public class SuccessEvaluator {

    private static final double INTERSECTION_RATIO_THRESH = 0.45;

    private final File annotationsFile;
    private final Map<NoteHeadAnnotation, Double> largestIntersectionRatioMap = new HashMap<>();

    public SuccessEvaluator(File annotationsFile) {
        this.annotationsFile = annotationsFile;
    }

    public String calculateNoteHeadSVMSuccesRate() {
        List<AnnotatedImage> parsedImages = AnnotationParser.parseAnnotations(annotationsFile, false);
        Map<NoteHeadType, NoteHeadStats> completeStats = new EnumMap<>(NoteHeadType.class);
        NoteHeadStats halfHeadStats = new NoteHeadStats();
        completeStats.put(NoteHeadType.HALF_HEAD, halfHeadStats);
        NoteHeadStats quarterHeadStats = new NoteHeadStats();
        completeStats.put(NoteHeadType.QUARTER_HEAD, quarterHeadStats);
        NoteHeadStats wholeHeadStats = new NoteHeadStats();
        completeStats.put(NoteHeadType.WHOLE_HEAD, wholeHeadStats);
        for (AnnotatedImage img : parsedImages) {
            List<NoteHeadAnnotation> detectionResults = img.runDetection();
            detectionResults.removeIf((NoteHeadAnnotation t) -> t.getType() == NoteHeadType.BACKGROUND);
            try {
                Map<NoteHeadType, NoteHeadStats> fileStats = evaluateFileResults(img);

                for (Map.Entry<NoteHeadType, NoteHeadStats> fileStatsEntry : fileStats.entrySet()) {
                    completeStats.get(fileStatsEntry.getKey()).addAnotherStats(fileStatsEntry.getValue());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return printStats(completeStats);
    }

    private String printStats(Map<NoteHeadType, NoteHeadStats> completeStats) {
        StringBuilder sb = new StringBuilder("Statistics of NoteHead SVM succes rate:\n");
        for (Map.Entry<NoteHeadType, NoteHeadStats> stats : completeStats.entrySet()) {
            double successRate;
            if (stats.getValue().expected == 0) {
                successRate = 100;
            } else {
                successRate = stats.getValue().found / (double) stats.getValue().expected;
            }
            sb.append("---------------------\n")
                    .append("NoteHead Type: ").append(stats.getKey().toString()).append("\n")
                    .append("Expected count to find: ").append(stats.getValue().getExpected()).append("\n")
                    .append("Found count: ").append(stats.getValue().getFound()).append("\n")
                    .append("Bad class detections: ").append(stats.getValue().getBadClassErrors()).append("\n")
                    .append("False positives: ").append(stats.getValue().getFalsePositives()).append("\n")
                    .append("Total incorrect detections: ").append((stats.getValue().getBadClassErrors()
                    + stats.getValue().getFalsePositives())).append("\n")
                    .append("Success percentage: ").append(String.format("%.2f", successRate * 100)).append("\n")
                    .append("---------------------").append("\n");
        }
        System.out.println(sb);
        return sb.toString();
    }

    private Map<NoteHeadType, NoteHeadStats> evaluateFileResults(AnnotatedImage img) {
        Map<NoteHeadType, NoteHeadStats> fileResults = new EnumMap<>(NoteHeadType.class);
        NoteHeadStats halfHeadStats = new NoteHeadStats();
        fileResults.put(NoteHeadType.HALF_HEAD, halfHeadStats);
        NoteHeadStats quarterHeadStats = new NoteHeadStats();
        fileResults.put(NoteHeadType.QUARTER_HEAD, quarterHeadStats);
        NoteHeadStats wholeHeadStats = new NoteHeadStats();
        fileResults.put(NoteHeadType.WHOLE_HEAD, wholeHeadStats);
        double detectionBboxSurface = img.getDetectionResults().get(0).getBBoxHighDPI().calculateSurface();
        processParsedNoteHeads(fileResults, img.getParsedAnnotations(), img.getDetectionResults(), detectionBboxSurface);
        //now we have just false-positives left (or incorrectly detected classes of noteheads)
        processIncorrectDetections(fileResults, img.getDetectionResults());
        return fileResults;
    }

    /**
     * Check success rate for each parsed notehead annotation.
     *
     * @param parsedAnnotations
     * @param detectedNoteHeads
     * @param detectionBboxSurface
     */
    private void processParsedNoteHeads(Map<NoteHeadType, NoteHeadStats> fileResults,
            List<NoteHeadAnnotation> parsedAnnotations, List<NoteHeadAnnotation> detectedNoteHeads,
            double detectionBboxSurface) {
        for (NoteHeadAnnotation annotation : parsedAnnotations) {
            NoteHeadStats noteHeadstats = fileResults.get(annotation.getType());
            switch (annotation.getType()) {
                case QUARTER_HEAD:
                    noteHeadstats.incExpectedCount();
                    if (resolveParsedAnnotation(annotation, detectedNoteHeads, detectionBboxSurface)) {
                        noteHeadstats.incFoundCount();
                    }
                    break;
                case HALF_HEAD:
                    noteHeadstats.incExpectedCount();
                    if (resolveParsedAnnotation(annotation, detectedNoteHeads, detectionBboxSurface)) {
                        noteHeadstats.incFoundCount();
                    }
                    break;
                case WHOLE_HEAD:
                    noteHeadstats.incExpectedCount();
                    if (resolveParsedAnnotation(annotation, detectedNoteHeads, detectionBboxSurface)) {
                        noteHeadstats.incFoundCount();
                    }
                    break;
            }
        }
    }

    /**
     * Sorts the incorrect detections into two categories (false positives and bad classes).
     *
     * @param fileResults
     * @param incorrectDetections
     */
    private void processIncorrectDetections(Map<NoteHeadType, NoteHeadStats> fileResults,
            List<NoteHeadAnnotation> incorrectDetections) {
        for (NoteHeadAnnotation incorrectDetection : incorrectDetections) {
            if (largestIntersectionRatioMap.getOrDefault(incorrectDetection, 0.0) > INTERSECTION_RATIO_THRESH) {
                //bad class error
                fileResults.get(incorrectDetection.getType()).incBadClassErrorCount();
            } else {
                //false positive
                fileResults.get(incorrectDetection.getType()).incFalsePositiveCount();
            }
        }
    }

    /**
     * Finds and removes the correctly detected bboxes. Correct in the sense of being the same note head type and sharing
     * more than 45% detected bbox surface.
     *
     * @param expectedAnnotation
     * @param detectionResults
     * @param detectionBboxSurface
     * @return true if at least one detected bbox corresponds to the parsed annotation
     */
    private boolean resolveParsedAnnotation(NoteHeadAnnotation expectedAnnotation,
            List<NoteHeadAnnotation> detectionResults, double detectionBboxSurface) {
        boolean foundSomething = false;
        for (Iterator<NoteHeadAnnotation> it = detectionResults.iterator(); it.hasNext();) {
            NoteHeadAnnotation detectedAnnotation = it.next();

            if (detectedAnnotation.getBBoxHighDPI().intersects(expectedAnnotation.getBBoxHighDPI())) {
                EnhancedRectangle intersection = new EnhancedRectangle(
                        detectedAnnotation.getBBoxHighDPI().intersection(expectedAnnotation.getBBoxHighDPI()));
                double intersectionSurface = intersection.width * intersection.height;
                if (largestIntersectionRatioMap.getOrDefault(detectedAnnotation, 0.0) < intersectionSurface) {
                    largestIntersectionRatioMap.put(detectedAnnotation, intersectionSurface);
                }
                if (detectedAnnotation.getType() == expectedAnnotation.getType()) {
                    if (intersectionSurface > (detectionBboxSurface * INTERSECTION_RATIO_THRESH)) {
                        foundSomething = true;
                        it.remove();
                    }
                }
            }
        }
        return foundSomething;
    }

    private static class NoteHeadStats {

        private int expected;
        private int found;
        private int badClassErrors;
        private int falsePositives;

        public void incExpectedCount() {
            expected++;
        }

        public void incFoundCount() {
            found++;
        }

        public void incExpectedCount(int i) {
            expected += i;
        }

        public void incFoundCount(int i) {
            found += i;
        }

        public void incBadClassErrorCount() {
            badClassErrors++;
        }

        public void incFalsePositiveCount() {
            falsePositives++;
        }

        public void addAnotherStats(NoteHeadStats otherStats) {
            this.expected += otherStats.expected;
            this.found += otherStats.found;
            this.badClassErrors += otherStats.badClassErrors;
            this.falsePositives += otherStats.falsePositives;
        }

        public int getExpected() {
            return expected;
        }

        public int getFound() {
            return found;
        }

        public int getBadClassErrors() {
            return badClassErrors;
        }

        public int getFalsePositives() {
            return falsePositives;
        }

    }
}
