package eu.dindoffer.omr.logic.dataset;

import eu.dindoffer.omr.logic.util.EnhancedRectangle;
import eu.dindoffer.omr.logic.notehead.NoteHeadType;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Martin Dindoffer
 */
public class AnnotationParser {

    public static List<AnnotatedImage> parseAnnotations(File annotationsFile, boolean includeBG) {
        List<AnnotatedImage> parsedImages = new LinkedList<>();
        String curImgPath = "";
        AnnotatedImage currentImg = null;
        int fileIDCounter = 0;
        try {
            List<String> lines = Files.readAllLines(annotationsFile.toPath());
            for (String line : lines) {
                String[] tokens = line.split(";");//relPath,x1,y1,x2,y2,classID
                if (!curImgPath.equals(tokens[0])) {
                    //if we step on a new annotated file
                    curImgPath = tokens[0];
                    File imageFile = new File(annotationsFile.getParent(), tokens[0]);
                    currentImg = new AnnotatedImage(imageFile, String.valueOf(fileIDCounter++));
                    parsedImages.add(currentImg);
                }
                NoteHeadType noteHeadType = parseAnnotationType(tokens[5]);
                if (includeBG || noteHeadType != NoteHeadType.BACKGROUND) {
                    //if we step on an actual non-BG annotation, or we want BG anyway
                    int width = Math.abs(Integer.parseInt(tokens[3]) - Integer.parseInt(tokens[1]));
                    int height = Math.abs(Integer.parseInt(tokens[4]) - Integer.parseInt(tokens[2]));
                    EnhancedRectangle anotatedBBox = new EnhancedRectangle(Integer.parseInt(tokens[1]), Integer.parseInt(
                            tokens[2]), width, height);
                    currentImg.addParsedAnnotation(anotatedBBox, noteHeadType);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return parsedImages;
    }

    private static NoteHeadType parseAnnotationType(String s) {
        switch (s) {
            case "quarternotehead":
                return NoteHeadType.QUARTER_HEAD;
            case "halfnotehead":
                return NoteHeadType.HALF_HEAD;
            case "wholenotehead":
                return NoteHeadType.WHOLE_HEAD;
            case "background":
                return NoteHeadType.BACKGROUND;
        }
        return null;
    }
}
