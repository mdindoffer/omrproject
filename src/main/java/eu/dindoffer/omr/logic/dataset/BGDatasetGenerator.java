package eu.dindoffer.omr.logic.dataset;

import static org.opencv.imgcodecs.Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE;

import eu.dindoffer.omr.logic.notehead.NoteHeadAnnotation;
import eu.dindoffer.omr.logic.notehead.NoteHeadType;
import eu.dindoffer.omr.logic.util.EnhancedRectangle;
import eu.dindoffer.omr.logic.util.Utils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import javax.imageio.ImageIO;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

/**
 *
 * Generates normalized dataset samples from provided annotated images. These samples are image areas, which were labeled as
 * foreground classes by a NoteHead detector, but were not present in the annotation file.
 *
 * @author Martin Dindoffer
 */
public class BGDatasetGenerator {

    private final int maximumSamples;
    private final Random samplePickIndexGenerator;
    private final File genDir;

    public BGDatasetGenerator(int maximumSamples) {
        this.maximumSamples = maximumSamples;
        this.samplePickIndexGenerator = new Random();
        this.genDir = new File(System.getProperty("user.dir"), "generated-samples");
    }

    public void createBGDataset(File annotations) {
        System.out.println("Creating BG dataset...");
        List<AnnotatedImage> parsedImages = AnnotationParser.parseAnnotations(annotations, false);
        int totalCount = 0;
        int sampleCount = 0;
        Map<AnnotatedImage, List<NoteHeadAnnotation>> resultsMap = new HashMap<>();
        for (AnnotatedImage img : parsedImages) {
            List<NoteHeadAnnotation> detectionResults = img.runDetection();
            totalCount += detectionResults.size();
            List<NoteHeadAnnotation> filteredResults = filterDetectionResults(detectionResults, img);
            sampleCount += filteredResults.size();
            resultsMap.put(img, filteredResults);
        }
        System.out.println("Total sample count before filtration: " + totalCount);
        System.out.println("Total sample count after filtration: " + sampleCount);
        generateSamples(resultsMap);
    }

    /**
     * Deletes every annotation from detection results, that shared at least 45% of the original annotation surface.
     *
     * @return filtered list of annotations
     */
    private List<NoteHeadAnnotation> filterDetectionResults(List<NoteHeadAnnotation> detectionResults,
            AnnotatedImage parsedImage) {
        List<NoteHeadAnnotation> filteredResults = new LinkedList<>(detectionResults);
        filteredResults.removeIf((NoteHeadAnnotation t) -> {
            return t.getType() == NoteHeadType.BACKGROUND;
        });
        double detectedBboxSurface = filteredResults.get(0).getBBoxHighDPI().calculateSurface();
        for (NoteHeadAnnotation parsedAnnotation : parsedImage.getParsedAnnotations()) {
            filteredResults.removeIf((NoteHeadAnnotation t) -> {
                EnhancedRectangle detectedBBox = t.getBBoxHighDPI();
                if (!detectedBBox.intersects(parsedAnnotation.getBBoxHighDPI())) {
                    return false;
                }
                EnhancedRectangle intersect = new EnhancedRectangle(detectedBBox.intersection(
                        parsedAnnotation.getBBoxHighDPI()));
                //if the intersection area is at least 45 percent of the detected BBox, we remove it
                double intersectionSurface = intersect.calculateSurface();
                double intersectionPercentage = intersectionSurface / detectedBboxSurface;
                System.out.println("Intersection percentage: " + String.format("%.2f", intersectionPercentage * 100));
                if (intersectionSurface > (detectedBboxSurface * 0.45)) {
                    return true;
                } else {
                    return false;
                }
            });
        }
        return filteredResults;
    }

    private void generateSamples(Map<AnnotatedImage, List<NoteHeadAnnotation>> resultsMap) {
        ArrayList<NoteHeadAnnotation> unifiedAnnotations = new ArrayList<>();
        Map<Range, AnnotatedImage> fileRanges = new HashMap<>();
        for (Entry<AnnotatedImage, List<NoteHeadAnnotation>> fileAnnotations : resultsMap.entrySet()) {
            Range indexRange = new Range(unifiedAnnotations.size(),
                    unifiedAnnotations.size() + fileAnnotations.getValue().size() - 1);
            unifiedAnnotations.addAll(fileAnnotations.getValue());
            fileRanges.put(indexRange, fileAnnotations.getKey());
        }
        Map<AnnotatedImage, Mat> filesToMatMap = new HashMap<>();
        for (AnnotatedImage img : resultsMap.keySet()) {
            Mat loadedImg = Imgcodecs.imread(img.getImageFile().getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
            Mat resizedMat = new Mat();
            Imgproc.resize(loadedImg, resizedMat, new Size(), img.getScalingFactor(), img.getScalingFactor(),
                    Imgproc.INTER_AREA);
            filesToMatMap.put(img, resizedMat);
        }
        Set<Integer> usedIndexes = new HashSet<>();
        int generatedSamples = 0;
        genDir.mkdir();
        while (generatedSamples < maximumSamples & usedIndexes.size() < unifiedAnnotations.size()) {
            Integer index = generateIndex(usedIndexes, unifiedAnnotations.size());
            usedIndexes.add(index);
            NoteHeadAnnotation annotation = unifiedAnnotations.get(index);
            AnnotatedImage annotatedImage = getAnnotatedImageForIndex(fileRanges, index);
            cutAndSaveSample(filesToMatMap.get(annotatedImage), annotation, annotatedImage.getFileID(), generatedSamples);
            generatedSamples++;
        }
    }

    private void cutAndSaveSample(Mat resizedMat, NoteHeadAnnotation annotation, String fileID, int sampleNumber) {
        Mat sub = resizedMat.submat((int) annotation.getTopLeftLowDPI().y, (int) annotation.getBottomRightLowDPI().y,
                (int) annotation.getTopLeftLowDPI().x, (int) annotation.getBottomRightLowDPI().x);
        File destination = new File(
                genDir.getAbsolutePath() + File.separator + "genBG_" + fileID + "_" + sampleNumber + "_"
                + System.currentTimeMillis() + ".png");
        try {
            ImageIO.write(Utils.toBufferedImage(sub), "png", destination);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Integer generateIndex(Set<Integer> usedIndexes, int indexBound) {
        Integer index = samplePickIndexGenerator.nextInt(indexBound);
        while (usedIndexes.contains(index)) {
            index = samplePickIndexGenerator.nextInt(indexBound);
        }
        return index;
    }

    private AnnotatedImage getAnnotatedImageForIndex(Map<Range, AnnotatedImage> fileRanges, int index) {
        for (Entry<Range, AnnotatedImage> entry : fileRanges.entrySet()) {
            if (entry.getKey().contains(index)) {
                return entry.getValue();
            }
        }
        return null;
    }

    /**
     * Inclusive integer range
     */
    private static class Range {

        private final int min;
        private final int max;

        public Range(int min, int max) {
            this.min = min;
            this.max = max;
        }

        public boolean contains(int value) {
            return value <= max && value >= min;
        }
    }
}
