package eu.dindoffer.omr.logic.dataset;

import static org.opencv.imgcodecs.Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE;

import eu.dindoffer.omr.logic.Page;
import eu.dindoffer.omr.logic.notehead.NoteHeadAnnotation;
import eu.dindoffer.omr.logic.notehead.NoteHeadType;
import eu.dindoffer.omr.logic.util.EnhancedRectangle;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

/**
 *
 * @author Martin Dindoffer
 */
public class AnnotatedImage {

    private final File imageFile;
    private final List<NoteHeadAnnotation> parsedAnnotations = new LinkedList<>();
    private List<NoteHeadAnnotation> detectionResults;
    private double scalingFactor;
    private final String fileID;

    public AnnotatedImage(File imageFile, String fileID) {
        this.imageFile = imageFile;
        this.fileID = fileID;
    }

    public void addParsedAnnotation(EnhancedRectangle rect, NoteHeadType noteHeadType) {
        parsedAnnotations.add(new NoteHeadAnnotation(rect, noteHeadType));
    }

    public List<NoteHeadAnnotation> runDetection() {
        Mat image = Imgcodecs.imread(imageFile.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
        Page page = new Page(image);
        page.calcVerticalLengthHistograms(45);
        detectionResults = page.detectNoteHeads(false, true);
        scalingFactor = page.getNoteHeadDetector().getScalingFactor();
        return detectionResults;
    }

    public List<NoteHeadAnnotation> getParsedAnnotations() {
        return parsedAnnotations;
    }

    public File getImageFile() {
        return imageFile;
    }

    public double getScalingFactor() {
        return scalingFactor;
    }

    public String getFileID() {
        return fileID;
    }

    public List<NoteHeadAnnotation> getDetectionResults() {
        return detectionResults;
    }
}
