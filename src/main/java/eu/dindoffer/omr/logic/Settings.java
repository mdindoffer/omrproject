package eu.dindoffer.omr.logic;

/**
 *
 * @author Martin Dindoffer
 */
public class Settings {

    public static int binarizationThreshold;
    public static boolean openingFirst;
    public static int openingMaskSize;
    public static int closingMaskSize;
    public static int maxVerticalLengthToTrack;
    public static double rho;
    public static double theta;
    public static int houghThreshold;
    public static int minLineSegmentLength;
    public static int maxLineSegmentGap;
    public static int maxLineSegmentAngleRotation;
    public static int windowingStepSize;
    public static double unificationThreshold;

    static {
        loadDefaults();
    }

    /**
     * Loads default values of algorithm parameters.
     */
    public static void loadDefaults() {
        binarizationThreshold = 150;
        openingFirst = true;
        openingMaskSize = 3;
        closingMaskSize = 3;
        maxVerticalLengthToTrack = 45;
        rho = 0.5;
        theta = 0.5;
        houghThreshold = 200;
        minLineSegmentLength = 80;
        maxLineSegmentGap = 5;
        maxLineSegmentAngleRotation = 15;
        windowingStepSize = 3;
        unificationThreshold = 0.5;
    }
}
