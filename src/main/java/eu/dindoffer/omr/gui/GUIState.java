package eu.dindoffer.omr.gui;

/**
 *
 * @author Martin Dindoffer
 */
public enum GUIState {
    APPLICATION_STARTED, FILE_LOADED, CALCULATION_RUNNING
}
