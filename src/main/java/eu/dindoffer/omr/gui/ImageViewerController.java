package eu.dindoffer.omr.gui;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author Martin Dindoffer
 */
public class ImageViewerController implements Initializable {

    @FXML
    private ImageView imvImage;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    public void setImage(Image img) {
        imvImage.setImage(img);
    }

}
