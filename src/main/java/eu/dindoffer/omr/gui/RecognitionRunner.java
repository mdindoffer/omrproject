package eu.dindoffer.omr.gui;

import eu.dindoffer.omr.gui.util.SexyExceptionAlert;
import eu.dindoffer.omr.gui.algorithmstepview.AlgorithmStep;
import eu.dindoffer.omr.gui.algorithmstepview.AlgorithmStep.AlgorithmStepStatus;
import eu.dindoffer.omr.gui.settings.SettingsBean;
import eu.dindoffer.omr.logic.Recognizer;
import eu.dindoffer.omr.logic.Settings;
import java.util.Collections;
import java.util.List;
import javafx.application.Platform;
import javafx.concurrent.Task;

/**
 *
 * @author Martin Dindoffer
 */
public class RecognitionRunner extends Task<Void> {

    private final Recognizer recognizer;
    private final List<AlgorithmStep> allSteps;
    private final MainWindowController controller;

    public RecognitionRunner(List<AlgorithmStep> steps, MainWindowController controller) {
        this.recognizer = Recognizer.getInstance();
        this.allSteps = steps;
        this.controller = controller;
    }

    @Override
    protected Void call() throws Exception {
        setRecognitionSettings();
        Collections.sort(allSteps);
        List<AlgorithmStep> stepsToResume = filterSteps(allSteps);
        int numOfIncludedSteps = 0;
        for (AlgorithmStep as : stepsToResume) {
            if (as.getStepInclusion()) {
                numOfIncludedSteps++;
            }
        }
        int numOfRanSteps = 0;
        for (AlgorithmStep as : stepsToResume) {
            if (isCancelled()) {
                cleanUpAfterCancel();
                break;
            }
            if (as.getStepInclusion()) {
                updateStepStatus(as, AlgorithmStepStatus.RUNNING);
                recognizer.runOperation(as.getOperation());
                numOfRanSteps++;
                updateStepStatus(as, AlgorithmStepStatus.DONE);
            } else {
                updateStepStatus(as, AlgorithmStepStatus.SKIPPED);
            }
            updateProgress(numOfRanSteps, numOfIncludedSteps);
        }
        return null;
    }

    /**
     * Returns steps that are positioned after the last completed operation, effectively doing a "resume".
     *
     * @param allSteps
     * @return
     */
    private List<AlgorithmStep> filterSteps(List<AlgorithmStep> allSteps) {
        int lastDoneIndex = -1;
        for (lastDoneIndex = allSteps.size() - 1; lastDoneIndex >= 0; lastDoneIndex--) {
            if (allSteps.get(lastDoneIndex).getStepStatus() == AlgorithmStepStatus.DONE) {
                //we found the last completed step
                break;
            }
        }
        if (lastDoneIndex == allSteps.size()) {
            return Collections.emptyList();
        }
        return allSteps.subList(lastDoneIndex + 1, allSteps.size());
    }

    private void cleanUpAfterCancel() {
        Platform.runLater(() -> {
            controller.setGUIState(GUIState.FILE_LOADED);
            controller.setProgressBarToZero();
        });
    }

    @Override
    protected void succeeded() {
        super.succeeded();
        controller.setGUIState(GUIState.FILE_LOADED);
        controller.setProgressBarToZero();
    }

    @Override
    protected void failed() {
        super.failed();
        SexyExceptionAlert sea = new SexyExceptionAlert(this.getException(),
                "Recognition failed!",
                "An error occured during the processing of the document...");
        sea.showAndWait();
    }

    private void updateStepStatus(AlgorithmStep step, AlgorithmStepStatus status) {
        Platform.runLater(() -> {
            step.setStepStatus(status);
        });
    }

    private void setRecognitionSettings() {
        Settings.binarizationThreshold = SettingsBean.getBinarizationThreshold();
        Settings.openingFirst = SettingsBean.getMorphologyOpeningFirst();
        Settings.openingMaskSize = SettingsBean.getMorphologyOpeningMaskSize();
        Settings.closingMaskSize = SettingsBean.getMorphologyClosingMaskSize();
        Settings.maxVerticalLengthToTrack = SettingsBean.getVerticalLengthHistMaxLengthToTrack();
        Settings.rho = SettingsBean.getHoughRho();
        Settings.theta = SettingsBean.getHoughTheta();
        Settings.houghThreshold = SettingsBean.getHoughThreshold();
        Settings.minLineSegmentLength = SettingsBean.getHoughMinLineLength();
        Settings.maxLineSegmentGap = SettingsBean.getHoughMaxLineGap();
        Settings.maxLineSegmentAngleRotation = SettingsBean.getHoughMaxSegmentAngle();
        Settings.windowingStepSize = SettingsBean.getNoteHeadWindowingStep();
        Settings.unificationThreshold = SettingsBean.getBboxUnificationThreshold();
    }
}
