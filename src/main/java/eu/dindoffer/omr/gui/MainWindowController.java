package eu.dindoffer.omr.gui;

import eu.dindoffer.omr.gui.util.TextAreaInfoAlert;
import eu.dindoffer.omr.gui.util.SexyExceptionAlert;
import eu.dindoffer.omr.gui.util.BusyTask;
import eu.dindoffer.omr.gui.algorithmstepview.AlgorithmStep;
import eu.dindoffer.omr.logic.ImageInfo;
import eu.dindoffer.omr.logic.Recognizer;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.fx.ChartViewer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * FXML Controller class
 *
 * @author Martin Dindoffer
 */
public class MainWindowController implements Initializable {

    private Recognizer recognizer;

    private FileChooser fileOpener;
    private FileChooser fileSaver;
    private FileChooser csvOpener;

    private File lastOpenDir = new File(System.getProperty("user.dir"));
    private File lastSaveDir = new File(System.getProperty("user.dir"));
    private File lastCSVDir = new File(System.getProperty("user.dir"));

    private GUIState guiState;

    @FXML
    private VBox myRoot;
    @FXML
    private Label lblName;
    @FXML
    private Label lblWidth;
    @FXML
    private Label lblHeight;
    @FXML
    private TableView<AlgorithmStep> tvAlgorithmSteps;
    @FXML
    private ProgressBar pbRecognitionProgress;
    @FXML
    private MenuBar mbarMainMenu;
    @FXML
    private MenuItem miOpenFile;
    @FXML
    private MenuItem miSaveCurrentResults;
    @FXML
    private MenuItem miOpenSettings;
    @FXML
    private MenuItem miShowOriginalImage;
    @FXML
    private MenuItem miShowCurrentResults;
    @FXML
    private MenuItem miShowVerticalLengthsHistogram;
    @FXML
    private MenuItem miTrainNoteHeadSVM;
    @FXML
    private MenuItem miCreateBGDataset;
    @FXML
    private Button btnRunPause;
    @FXML
    private Menu mnuFile;
    @FXML
    private Menu mnuShow;
    @FXML
    private Menu mnuOther;

    private RecognitionRunner recognitionRunner;
    @FXML
    private MenuItem miRateNoteHeadSVM;
    @FXML
    private MenuItem miCalcNHConfusionMatrix;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.recognizer = Recognizer.getInstance();
        this.fileOpener = new FileChooser();
        fileOpener.setTitle("Open a scanned music score");
        fileOpener.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", ImageExtensionFilter.getAllImagesExtensions()),
                new FileChooser.ExtensionFilter("JPG", ImageExtensionFilter.JPEG_EXTS.getFileExtensions()),
                new FileChooser.ExtensionFilter("PNG", ImageExtensionFilter.PNG_EXTS.getFileExtensions()),
                new FileChooser.ExtensionFilter("BMP", ImageExtensionFilter.WIN_BITMAPS_EXTS.getFileExtensions())
        );
        this.fileSaver = new FileChooser();
        fileSaver.setTitle("Save current results as an image");
        fileSaver.getExtensionFilters().add(new FileChooser.ExtensionFilter("PNG",
                ImageExtensionFilter.PNG_EXTS.getFileExtensions()));
        csvOpener = new FileChooser();
        csvOpener.setTitle("Select an annotations CSV file");
        csvOpener.getExtensionFilters().add((new FileChooser.ExtensionFilter("CSV", "*.csv", "*.CSV")));

        ObservableList<AlgorithmStep> stepList = AlgorithmStep.createAlgorithmSteps();
        AlgorithmStep.updateEventProvider().addListener(
                (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                    tvAlgorithmSteps.refresh();
                });
        tvAlgorithmSteps.setItems(stepList);
        setGUIState(GUIState.APPLICATION_STARTED);
    }

    @FXML
    private void onMenuOpenClick(ActionEvent event) {
        fileOpener.setInitialDirectory(lastOpenDir);
        final File chosenFile = fileOpener.showOpenDialog(myRoot.getScene().getWindow());
        if (chosenFile != null) {
            lastOpenDir = chosenFile.getParentFile();
            Task<ImageInfo> task = new Task<ImageInfo>() {
                @Override
                protected ImageInfo call() throws Exception {
                    return recognizer.loadImage(chosenFile);
                }
            };
            task.setOnSucceeded((WorkerStateEvent t) -> {
                ImageInfo result = task.getValue();
                lblHeight.setText(String.valueOf(result.height));
                lblWidth.setText(String.valueOf(result.width));
                lblName.setText(chosenFile.getName());
                cleanStepStatuses();
                setGUIState(GUIState.FILE_LOADED);
            });
            task.setOnFailed((WorkerStateEvent t) -> {
                SexyExceptionAlert sea = new SexyExceptionAlert(task.getException(),
                        "Image opening failed!",
                        "Please note, that OpenCV is a huge pile of shit and its Windows binaries cannot open files with"
                        + " non-ASCII character paths");
                sea.showAndWait();
            });

            new Thread(task).start();
        }
    }

    @FXML
    private void onMenuSaveCurrentResultsClick() throws IOException {
        fileSaver.setInitialDirectory(lastSaveDir);
        final File chosenDestination = fileSaver.showSaveDialog(myRoot.getScene().getWindow());
        if (chosenDestination != null) {
            lastSaveDir = chosenDestination.getParentFile();
            BusyTask busyTask = new BusyTask("Saving results to a file...", "Image saving failed!", null) {
                @Override
                protected Void call() throws Exception {
                    BufferedImage displayableVisualResult = recognizer.getDisplayableVisualResult();
                    if (!chosenDestination.getName().endsWith(".png") && !chosenDestination.getName().endsWith(".PNG")) {
                        ImageIO.write(displayableVisualResult, "png", new File(chosenDestination + ".png"));
                    } else {
                        ImageIO.write(displayableVisualResult, "png", chosenDestination);
                    }
                    return null;
                }
            };
            new Thread(busyTask).start();
        }
    }

    @FXML
    private void onMenuTrainNoteHeadSVM(ActionEvent event) {
        DirectoryChooser dirChooser = new DirectoryChooser();
        dirChooser.setInitialDirectory(lastOpenDir);
        dirChooser.setTitle("Select a directory containing NoteHead dataset");
        final File chosenDir = dirChooser.showDialog(myRoot.getScene().getWindow());
        if (chosenDir != null) {
            BusyTask busyTask = new BusyTask("Training the SVM...", "SVM training failed!",
                    "Are you sure, you have selected a folder with the correct subfolder structure?\n"
                    + "The chosen folder should contain folders named "
                    + "background, quarternotehead, wholenotehead, and halfnotehead.") {
                @Override
                protected Void call() throws Exception {
                    recognizer.trainNoteHeadSVM(chosenDir);
                    return null;
                }
            };
            new Thread(busyTask).start();
        }
    }

    @FXML
    private void onMenuCreateBGDataset(ActionEvent event) {
        csvOpener.setInitialDirectory(lastCSVDir);
        final File chosenFile = csvOpener.showOpenDialog(myRoot.getScene().getWindow());
        if (chosenFile != null) {
            lastCSVDir = chosenFile.getParentFile();
            TextInputDialog dialog = new TextInputDialog();
            final TextField editor = dialog.getEditor();
            editor.textProperty().addListener(
                    (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        if (!newValue.matches("\\d*")) {
                            editor.setText(newValue.replaceAll("[^\\d]", ""));
                        }
                    });
            editor.setText("100");
            dialog.setTitle("(Maximum) Number of samples to generate");
            dialog.setContentText("(Maximum) Number of samples to generate");
            dialog.setHeaderText(null);
            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()) {
                BusyTask busyTask = new BusyTask("Creating BG dataset...",
                        "Background sample generation failed!", null) {
                    @Override
                    protected Void call() throws Exception {
                        recognizer.createBGDataset(chosenFile, Integer.parseInt(result.get()));
                        return null;
                    }
                };
                new Thread(busyTask).start();
            }
        }
    }

    @FXML
    private void onMenuRateNoteHeadSVM(ActionEvent event) {
        csvOpener.setInitialDirectory(lastCSVDir);
        final File chosenFile = csvOpener.showOpenDialog(myRoot.getScene().getWindow());
        if (chosenFile != null) {
            lastCSVDir = chosenFile.getParentFile();

            BusyTask<String> busyTask = new BusyTask<String>("Evaluating SVM NoteHead model...",
                    "NoteHead detection evaluation failed!", null) {
                @Override
                protected String call() throws Exception {
                    return recognizer.calculateNoteHeadSVMSuccesRate(chosenFile);
                }

                @Override
                protected void succeeded() {
                    super.succeeded();
                    TextAreaInfoAlert taia = new TextAreaInfoAlert("SVM success evaluation", this.getValue(), 400, 500);
                    taia.show();
                }
            };
            new Thread(busyTask).start();
        }
    }

    @FXML
    private void onMenuCalcNHConfusionMatrix(ActionEvent event) {
        DirectoryChooser dirChooser = new DirectoryChooser();
        dirChooser.setInitialDirectory(lastOpenDir);
        dirChooser.setTitle("Select a directory containing NoteHead dataset");
        final File chosenDir = dirChooser.showDialog(myRoot.getScene().getWindow());
        if (chosenDir != null) {
            BusyTask<String> busyTask = new BusyTask<String>("Creating the confusion matrix...",
                    "Confusion matrix calculation failed!",
                    "Are you sure, you have selected a folder with the correct subfolder structure?\n"
                    + "The chosen folder should contain folders named "
                    + "background, quarternotehead, wholenotehead, and halfnotehead.") {
                @Override
                protected String call() throws Exception {
                    return recognizer.evaluatePlainNoteHeadSamples(chosenDir);
                }

                @Override
                protected void succeeded() {
                    super.succeeded();
                    TextAreaInfoAlert taia = new TextAreaInfoAlert("Confusion matrix", this.getValue(), 550, 200);
                    taia.show();
                }
            };
            new Thread(busyTask).start();
        }
    }

    @FXML
    private void onMenuShowOriginalClick(ActionEvent event) throws IOException {
        openImageViewer("Original image", recognizer.getDisplayableOriginal());
    }

    @FXML
    private void onMenuShowCurrentResultsClick(ActionEvent event) throws IOException {
        openImageViewer("Results of the last step", recognizer.getDisplayableVisualResult());
    }

    @FXML
    private void onMenuShowVerticalLengthsHistogram(ActionEvent event) {
        XYSeries blackSeries = new XYSeries("Blacks");
        int[] blacks = recognizer.getBlackVerticalLengthHistogram();
        for (int i = 0; i < blacks.length; i++) {
            blackSeries.add(i, blacks[i]);
        }

        XYSeries whiteSeries = new XYSeries("Whites");
        int[] whites = recognizer.getWhiteVerticalLengthHistogram();
        for (int i = 0; i < whites.length; i++) {
            whiteSeries.add(i, whites[i]);
        }

        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(blackSeries);
        dataset.addSeries(whiteSeries);

        JFreeChart chart = ChartFactory.createXYLineChart("Vertical lengths histograms", "Lengths", "Count", dataset);
        chart.getXYPlot().setDomainPannable(true);
        chart.getXYPlot().setRangePannable(true);
        ChartViewer viewer = new ChartViewer(chart);
        Stage stage = new Stage();
        stage.setScene(new Scene(viewer));
        stage.setTitle("Vertical Lengths Histograms");
        stage.setWidth(600);
        stage.setHeight(300);
        stage.show();
    }

    private void openImageViewer(String title, BufferedImage bufferedImage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ImageViewer.fxml"));
        Parent root = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.setMaximized(true);
        stage.setTitle(title);
        Image image = SwingFXUtils.toFXImage(bufferedImage, null);
        ImageViewerController ivController = fxmlLoader.<ImageViewerController>getController();
        ivController.setImage(image);
        stage.setScene(new Scene(root));
        stage.show();
    }

    @FXML
    private void onMenuSettingsClick(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("SettingsWindow.fxml"));
        Parent root = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.initOwner(myRoot.getScene().getWindow());
        stage.initModality(Modality.WINDOW_MODAL);
        stage.setTitle("Settings");
        stage.setScene(new Scene(root));
        stage.show();
    }

    @FXML
    private void onBtnRunPauseClick(ActionEvent event) {
        switch (guiState) {
            case FILE_LOADED:
                setGUIState(GUIState.CALCULATION_RUNNING);
                recognitionRunner = new RecognitionRunner(new LinkedList<>(tvAlgorithmSteps.getItems()), this);
                pbRecognitionProgress.progressProperty().bind(recognitionRunner.progressProperty());
                new Thread(recognitionRunner).start();
                break;
            case CALCULATION_RUNNING:
                btnRunPause.setDisable(true);
                pbRecognitionProgress.progressProperty().unbind();
                pbRecognitionProgress.progressProperty().set(-1);
                recognitionRunner.cancel(true);
                break;
        }
    }

    public void setProgressBarToZero() {
        pbRecognitionProgress.progressProperty().unbind();
        pbRecognitionProgress.progressProperty().set(0);
    }

    public void cleanStepStatuses() {
        for (AlgorithmStep as : tvAlgorithmSteps.getItems()) {
            as.setStepStatus(AlgorithmStep.AlgorithmStepStatus.UNSCHEDULED);
        }
    }

    public void setGUIState(GUIState state) {
        this.guiState = state;
        switch (state) {
            case APPLICATION_STARTED:
                //menus
                miOpenFile.setDisable(false);
                miShowCurrentResults.setDisable(true);
                miOpenSettings.setDisable(false);
                mnuShow.setDisable(true);
                mnuOther.setDisable(false);
                miTrainNoteHeadSVM.setDisable(false);
                miCreateBGDataset.setDisable(false);
                //controls
                tvAlgorithmSteps.setDisable(true);
                tvAlgorithmSteps.setEditable(false);
                btnRunPause.setDisable(true);
                break;
            case FILE_LOADED:
                mbarMainMenu.setDisable(false);
                mnuShow.setDisable(false);
                btnRunPause.setDisable(false);
                tvAlgorithmSteps.setDisable(false);
                tvAlgorithmSteps.setEditable(true);
                //bind run-specific things to algorithm step completion
                miShowCurrentResults.setDisable(!AlgorithmStep.isThresholdOperationCompleted());
                miSaveCurrentResults.setDisable(!AlgorithmStep.isThresholdOperationCompleted());
                miShowVerticalLengthsHistogram.setDisable(!AlgorithmStep.isVerticalLengthHistogramCompleted());
                break;
            case CALCULATION_RUNNING:
                mbarMainMenu.setDisable(true);
                tvAlgorithmSteps.setEditable(false);
                break;
            default:
                throw new AssertionError(state.name());
        }
    }
}
