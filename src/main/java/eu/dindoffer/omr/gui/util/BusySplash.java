package eu.dindoffer.omr.gui.util;

import javafx.geometry.Pos;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.VBox;
import javafx.stage.StageStyle;

/**
 *
 * @author Martin Dindoffer
 */
public class BusySplash extends Dialog<Void> {

    public BusySplash(String text) {
        super();
        ProgressIndicator pi = new ProgressIndicator();
        this.setGraphic(pi);
        this.initStyle(StageStyle.UNDECORATED);
        this.setHeaderText(null);
        Label lbl = new Label(text);
        lbl.setWrapText(true);
        VBox xbox360 = new VBox(lbl);
        xbox360.setAlignment(Pos.CENTER);
        this.getDialogPane().setContent(xbox360);
    }

}
