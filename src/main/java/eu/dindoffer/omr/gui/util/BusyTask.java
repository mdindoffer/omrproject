package eu.dindoffer.omr.gui.util;

import javafx.concurrent.Task;
import javafx.scene.control.ButtonType;

/**
 *
 * @author Martin Dindoffer
 */
public abstract class BusyTask<V> extends Task<V> {

    private final BusySplash splash;
    private final String predefErrorTitle;
    private final String predefErrorDetails;

    public BusyTask(String busyText, String predefErrorTitle, String predefErrorDetails) {
        this.splash = new BusySplash(busyText);
        this.predefErrorTitle = predefErrorTitle;
        this.predefErrorDetails = predefErrorDetails;
    }

    @Override
    protected void running() {
        super.running();
        splash.showAndWait();
    }

    @Override
    protected void succeeded() {
        super.succeeded();
        closeSplash();
    }

    @Override
    protected void failed() {
        super.failed();
        SexyExceptionAlert sea = new SexyExceptionAlert(this.getException(),
                predefErrorTitle, predefErrorDetails);
        sea.showAndWait();
        closeSplash();
    }

    private void closeSplash() {
        //we need to add a cancel button to be able to call close()
        //trivia: did you know "JavaFX" means "Fuck logic" in ancient arabic?
        splash.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        splash.close();
    }
}
