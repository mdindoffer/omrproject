package eu.dindoffer.omr.gui.util;

import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.text.Font;

/**
 *
 * @author Martin Dindoffer
 */
public class TextAreaInfoAlert extends Alert {

    public TextAreaInfoAlert(String title, String textContent, double prefWidth, double prefHeight) {
        super(AlertType.INFORMATION);
        this.setGraphic(null);
        this.setTitle(title);
        this.setHeaderText(null);
        TextArea textArea = new TextArea(textContent);
        textArea.setFont(Font.font("Monospaced"));
        this.getDialogPane().setContent(textArea);
        this.setResizable(true);
        this.getDialogPane().setPrefSize(prefWidth, prefHeight);
    }

}
