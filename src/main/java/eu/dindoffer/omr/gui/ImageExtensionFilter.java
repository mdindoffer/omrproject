package eu.dindoffer.omr.gui;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Martin Dindoffer
 */
public enum ImageExtensionFilter {
    WIN_BITMAPS_EXTS("*.bmp", "*.BMP", "*.dib", "*.DIB"),
    JPEG_EXTS("*.jpeg", "*.JPEG", "*.jpg", "*.JPG", "jpe", "*.JPE"),
    JPEG2000_EXTS("*.jp2", "*.JP2"),
    PNG_EXTS("*.png", "*.PNG"),
    NETPBM_EXTS("*.pbm", "*.PBM", "*.pgm", "*.PGM", "*.ppm", "*.PPM"),
    SUN_EXTS("*.sr", "*.SR", "*.ras", "*.RAS"),
    TIFF_EXTS("*.tiff", "*.TIFF", "*.tif", "*.TIFF");

    private final List<String> fileExtensions;

    private ImageExtensionFilter(String... extension) {
        this.fileExtensions = (Arrays.asList(extension));
    }

    private static final List<String> ALL_IMAGES_EXTS = aggregateAllExtensions();

    public List<String> getFileExtensions() {
        return fileExtensions;
    }

    public static List<String> getAllImagesExtensions() {
        return ALL_IMAGES_EXTS;
    }

    private static List<String> aggregateAllExtensions() {
        List<String> exts = new LinkedList<>();
        for (ImageExtensionFilter filter : ImageExtensionFilter.values()) {
            exts.addAll(filter.getFileExtensions());
        }
        return exts;
    }
}
