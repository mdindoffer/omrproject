package eu.dindoffer.omr.gui.algorithmstepview;

import eu.dindoffer.omr.logic.Operation;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Martin Dindoffer
 */
public final class AlgorithmStep implements Comparable<AlgorithmStep> {

    //holds the operations/steps included in the pipeline
    public static final Set<Operation> INCLUDED_OPERATIONS = new HashSet<>();

    static {
        INCLUDED_OPERATIONS.add(Operation.OPEN);
    }

    public static Map<Operation, AlgorithmStep> STEP_MAP;

    private final List<AlgorithmStep> dependencyDescendants = new LinkedList<>();

    //this property enables the view to listen to pseudo-atomic changes and change its update/redrawing strategy
    private static final SimpleBooleanProperty UPDATE_EVENT_PROVIDER = new SimpleBooleanProperty(false);

    private final SimpleIntegerProperty stepOrder = new SimpleIntegerProperty(0);
    private final SimpleStringProperty stepName = new SimpleStringProperty("");
    private final SimpleBooleanProperty stepInclusion = new SimpleBooleanProperty(false);
    private final SimpleObjectProperty<AlgorithmStepStatus> stepStatus = new SimpleObjectProperty<>(AlgorithmStepStatus.UNSCHEDULED);

    private final Operation operation;

    private AlgorithmStep(Operation operation, int orderNum, String stepName) {
        this.operation = operation;
        setStepOrder(orderNum);
        setStepName(stepName);

        stepInclusion.addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue == true) {
                    INCLUDED_OPERATIONS.add(operation);
                    //notify the view to update the list of steps
                    notifyView();
                } else {
                    removeStep();
                    //now, after recursion has ended, notify view to update the list of steps
                    notifyView();
                }
            }
        });
    }

    private void removeStep() {
        INCLUDED_OPERATIONS.remove(this.operation);
        this.setStepInclusion(false);
        for (AlgorithmStep as : dependencyDescendants) {
            as.removeStep();
        }
    }

    private static void notifyView() {
        UPDATE_EVENT_PROVIDER.set(!UPDATE_EVENT_PROVIDER.get());
    }

    public static ObservableList<AlgorithmStep> createAlgorithmSteps() {
        List<AlgorithmStep> stepList = new LinkedList<>();
        STEP_MAP = new HashMap<>();
        for (int i = 0; i < Operation.PIPELINE_ORDERING.size(); i++) {
            Operation stepOperation = Operation.PIPELINE_ORDERING.get(i);
            AlgorithmStep as = new AlgorithmStep(stepOperation, i, "");
            assignStepName(as);
            stepList.add(as);
            STEP_MAP.put(stepOperation, as);
        }
        buildDependencyChains(STEP_MAP);

        initDefaultPipeline(stepList);
        return FXCollections.observableArrayList(stepList.subList(1, stepList.size()));
    }

    private static void buildDependencyChains(Map<Operation, AlgorithmStep> stepMap) {
        for (Entry<Operation, AlgorithmStep> step : stepMap.entrySet()) {
            Operation operationAncestor = step.getValue().operation.getDependentOperation();
            if (operationAncestor != null) {
                stepMap.get(operationAncestor).addDependencyDescendant(step.getValue());
            }
        }
    }

    private static void assignStepName(AlgorithmStep as) {
        switch (as.operation) {
            case BINARIZE:
                as.setStepName("Threshold binarization");
                break;
            case DENOISE:
                as.setStepName("Morphological noise reduction");
                break;
            case CALCULATE_VERTICAL_LENGTHS:
                as.setStepName("Vertical lengths histograms");
                break;
            case PROJECT_HISTOGRAMS:
                as.setStepName("Projection histograms");
                break;
            case HOUGH_TRANSFORM:
                as.setStepName("Hough Transformation");
                break;
            case SKELETONIZE:
                as.setStepName("Zhang-Suen skeletonization");
                break;
            case FIND_POLYLINES:
                as.setStepName("Polyline search");
                break;
            case FIND_STAFF_LINES:
                as.setStepName("Staff line search");
                break;
            case REMOVE_STAFF_LINES:
                as.setStepName("Staff line removal");
                break;
            case DETECT_NOTE_HEADS:
                as.setStepName("Note head SVM detection");
                break;
            case UNIFY_NH_ANNOTATIONS:
                as.setStepName("Intersection over union");
                break;
            case RESOLVE_NH_MULTIPLICITIES:
                as.setStepName("Resolve note head multiplicities");
                break;
            default:
                as.setStepName("Unknown operation: " + as.operation.name());
        }
    }

    private static void initDefaultPipeline(List<AlgorithmStep> steps) {
        for (AlgorithmStep as : steps) {
            switch (as.operation) {
                case BINARIZE:
                    as.setStepInclusion(true);
                    break;
                case DENOISE:
                    as.setStepInclusion(true);
                    break;
                case CALCULATE_VERTICAL_LENGTHS:
                    as.setStepInclusion(true);
                    break;
                case PROJECT_HISTOGRAMS:
                    break;
                case HOUGH_TRANSFORM:
                    as.setStepInclusion(true);
                    break;
                case SKELETONIZE:
                    as.setStepInclusion(true);
                    break;
                case FIND_POLYLINES:
                    as.setStepInclusion(true);
                    break;
                case FIND_STAFF_LINES:
                    as.setStepInclusion(true);
                    break;
                case REMOVE_STAFF_LINES:
                    break;
                case DETECT_NOTE_HEADS:
                    as.setStepInclusion(true);
                    break;
                default:
            }
        }
    }

    /**
     * Checks whether this step can be included in the pipeline (with respect to operation pipeline ordering and dependency
     * tree)
     *
     * @return true if the step can be included, false otherwise
     */
    public boolean canBeIncluded() {
        return INCLUDED_OPERATIONS.contains(this.operation.getDependentOperation());
    }

    public static boolean isThresholdOperationCompleted() {
        if (STEP_MAP.get(Operation.BINARIZE).getStepStatus() == AlgorithmStepStatus.DONE) {
            return true;
        }
        return false;
    }

    public static boolean isProjectionHistogramCompleted() {
        if (STEP_MAP.get(Operation.PROJECT_HISTOGRAMS).getStepStatus() == AlgorithmStepStatus.DONE) {
            return true;
        }
        return false;
    }

    public static boolean isVerticalLengthHistogramCompleted() {
        if (STEP_MAP.get(Operation.CALCULATE_VERTICAL_LENGTHS).getStepStatus() == AlgorithmStepStatus.DONE) {
            return true;
        }
        return false;
    }

    private void addDependencyDescendant(AlgorithmStep descendant) {
        dependencyDescendants.add(descendant);
    }

    public int getStepOrder() {
        return stepOrder.get();
    }

    public void setStepOrder(int order) {
        stepOrder.set(order);
    }

    public IntegerProperty stepOrder() {
        return stepOrder;
    }

    public String getStepName() {
        return stepName.get();
    }

    public void setStepName(String stepName) {
        this.stepName.set(stepName);
    }

    public boolean getStepInclusion() {
        return stepInclusion.get();
    }

    public void setStepInclusion(boolean stepInclusion) {
        this.stepInclusion.set(stepInclusion);
    }

    public BooleanProperty stepInclusionProperty() {
        return stepInclusion;
    }

    public ObjectProperty<AlgorithmStepStatus> stepStatusProperty() {
        return stepStatus;
    }

    public AlgorithmStepStatus getStepStatus() {
        return stepStatus.get();
    }

    public void setStepStatus(AlgorithmStepStatus stepStatus) {
        this.stepStatus.set(stepStatus);
    }

    public static BooleanProperty updateEventProvider() {
        return UPDATE_EVENT_PROVIDER;
    }

    public static void setUpdateEventProvider(boolean something) {
        UPDATE_EVENT_PROVIDER.set(something);
    }

    public static boolean getUpdateEventProvider() {
        return UPDATE_EVENT_PROVIDER.get();
    }

    public Operation getOperation() {
        return operation;
    }

    @Override
    public int compareTo(AlgorithmStep another) {
        return this.getStepOrder() - another.getStepOrder();
    }

    public enum AlgorithmStepStatus {
        UNSCHEDULED(""), SCHEDULED("Scheduled"), SKIPPED("Skipped"), RUNNING("Running"), DONE("Done");

        private final String text;

        private AlgorithmStepStatus(String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return this.text;
        }
    }
}
