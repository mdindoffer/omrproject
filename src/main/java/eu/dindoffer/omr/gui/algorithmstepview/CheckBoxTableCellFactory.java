package eu.dindoffer.omr.gui.algorithmstepview;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.util.Callback;

/**
 *
 * @author Martin Dindoffer
 */
public class CheckBoxTableCellFactory<S, T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {

    @Override
    public TableCell<S, T> call(TableColumn<S, T> param) {
        CheckBoxTableCell<S, T> checkBoxTableCell = new CheckBoxTableCell<S, T>() {

            @Override
            public void updateItem(T item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    TableRow tableRow = getTableRow();
                    AlgorithmStep as = (AlgorithmStep) tableRow.getItem();
                    if (as != null) {
                        if (as.canBeIncluded()) {
                            setDisable(false);
                            tableRow.setOpacity(1);
                        } else {
                            setDisable(true);
                            tableRow.setOpacity(0.5);
                        }
                    }
                }
            }
        };
        return checkBoxTableCell;
    }
}
