package eu.dindoffer.omr.gui.settings;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.ObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.control.ToggleGroup;

/**
 * FXML Controller class
 *
 * @author Martin Dindoffer
 */
public class SettingsWindowController implements Initializable {

    @FXML
    private ToggleGroup tgMorphologyOrder;
    @FXML
    private Spinner<Integer> spnOpeningMask;
    @FXML
    private Spinner<Integer> spnClosingMask;
    @FXML
    private Spinner<Integer> spnMaxLengthToTrack;
    @FXML
    private Slider sldBinarizationThreshold;
    @FXML
    private RadioButton rbOpeningFirst;
    @FXML
    private RadioButton rbClosingFirst;
    @FXML
    private Spinner<Double> spnHoughRho;
    @FXML
    private Spinner<Double> spnHoughTheta;
    @FXML
    private Spinner<Integer> spnHoughThreshold;
    @FXML
    private Slider sldHoughMinLineLength;
    @FXML
    private Slider sldHoughMaxLineGap;
    @FXML
    private Slider sldHoughMaxLineAngle;
    @FXML
    private Spinner<Integer> spnNoteHeadWindowingStep;
    @FXML
    private Slider sldIntersectionOverUnionThreshold;

    //I have no idea, why these fields need to be static for the bindings to work
    //If someone does know why, please let me know
    private static final ObjectProperty<Integer> morphOpeningMaskSizeObjectProperty = SettingsBean
            .morphologyOpeningMaskSizeProperty().asObject();
    private static final ObjectProperty<Integer> morphClosingMaskSizeObjectProperty = SettingsBean
            .morphologyClosingMaskSizeProperty().asObject();
    private static final ObjectProperty<Integer> maxLengthToTrackObjectProperty = SettingsBean
            .verticalLengthHistMaxLengthToTrackProperty().asObject();
    private static final ObjectProperty<Double> houghRhoObjectProperty = SettingsBean
            .houghRhoProperty().asObject();
    private static final ObjectProperty<Double> houghThetaObjectProperty = SettingsBean
            .houghThetaProperty().asObject();
    private static final ObjectProperty<Integer> houghThresholdObjectProperty = SettingsBean
            .houghThresholdProperty().asObject();
    private static final ObjectProperty<Integer> noteHeadWindowingStepObjectProperty = SettingsBean
            .noteHeadWindowingStepProperty().asObject();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //init bindings for controls
        sldBinarizationThreshold.valueProperty().bindBidirectional(SettingsBean.binarizationThresholdProperty());
        rbOpeningFirst.selectedProperty().bindBidirectional(SettingsBean.morphologyOpeningFirstProperty());
        //make always one of the toggle buttons selected
        rbClosingFirst.setSelected(!rbOpeningFirst.isSelected());
        spnOpeningMask.getValueFactory().valueProperty().bindBidirectional(morphOpeningMaskSizeObjectProperty);
        spnClosingMask.getValueFactory().valueProperty().bindBidirectional(morphClosingMaskSizeObjectProperty);
        spnMaxLengthToTrack.getValueFactory().valueProperty().bindBidirectional(maxLengthToTrackObjectProperty);
        spnHoughRho.getValueFactory().valueProperty().bindBidirectional(houghRhoObjectProperty);
        spnHoughTheta.getValueFactory().valueProperty().bindBidirectional(houghThetaObjectProperty);
        spnHoughThreshold.getValueFactory().valueProperty().bindBidirectional(houghThresholdObjectProperty);
        sldHoughMinLineLength.valueProperty().bindBidirectional(SettingsBean.houghMinLineLengthProperty());
        sldHoughMaxLineGap.valueProperty().bindBidirectional(SettingsBean.houghMaxLineGapProperty());
        sldHoughMaxLineAngle.valueProperty().bindBidirectional(SettingsBean.houghMaxSegmentAngleProperty());
        spnNoteHeadWindowingStep.getValueFactory().valueProperty().bindBidirectional(noteHeadWindowingStepObjectProperty);
        sldIntersectionOverUnionThreshold.valueProperty().bindBidirectional(SettingsBean.bboxUnificationThreshold());
    }
}
