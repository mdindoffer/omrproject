package eu.dindoffer.omr.gui.settings;

import eu.dindoffer.omr.logic.Settings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Martin Dindoffer
 */
public class SettingsBean {

    private static final IntegerProperty binarizationThreshold = new SimpleIntegerProperty(Settings.binarizationThreshold);
    private static final BooleanProperty morphologyOpeningFirst = new SimpleBooleanProperty(Settings.openingFirst);
    private static final IntegerProperty morphologyOpeningMaskSize = new SimpleIntegerProperty(Settings.openingMaskSize);
    private static final IntegerProperty morphologyClosingMaskSize = new SimpleIntegerProperty(Settings.closingMaskSize);
    private static final IntegerProperty verticalLengthHistMaxLengthToTrack = new SimpleIntegerProperty(Settings.maxVerticalLengthToTrack);
    private static final DoubleProperty houghRho = new SimpleDoubleProperty(Settings.rho);
    private static final DoubleProperty houghTheta = new SimpleDoubleProperty(Settings.theta);
    private static final IntegerProperty houghThreshold = new SimpleIntegerProperty(Settings.houghThreshold);
    private static final IntegerProperty houghMinLineLength = new SimpleIntegerProperty(Settings.minLineSegmentLength);
    private static final IntegerProperty houghMaxLineGap = new SimpleIntegerProperty(Settings.maxLineSegmentGap);
    private static final IntegerProperty houghMaxSegmentAngle = new SimpleIntegerProperty(Settings.maxLineSegmentAngleRotation);
    private static final IntegerProperty noteHeadWindowingStep = new SimpleIntegerProperty(Settings.windowingStepSize);
    private static final DoubleProperty bboxUnificationThreshold = new SimpleDoubleProperty(Settings.unificationThreshold);

    public static IntegerProperty binarizationThresholdProperty() {
        return binarizationThreshold;
    }

    public static Integer getBinarizationThreshold() {
        return binarizationThreshold.getValue();
    }

    public static void setBinarizationThreshold(int threshold) {
        binarizationThreshold.set(threshold);
    }

    public static BooleanProperty morphologyOpeningFirstProperty() {
        return morphologyOpeningFirst;
    }

    public static Boolean getMorphologyOpeningFirst() {
        return morphologyOpeningFirst.get();
    }

    public static void setMorphologyOpeningFirst(boolean value) {
        morphologyOpeningFirst.set(value);
    }

    public static IntegerProperty morphologyOpeningMaskSizeProperty() {
        return morphologyOpeningMaskSize;
    }

    public static Integer getMorphologyOpeningMaskSize() {
        return morphologyOpeningMaskSize.getValue();
    }

    public static void setMorphologyOpeningMaskSize(int value) {
        morphologyOpeningMaskSize.set(value);
    }

    public static IntegerProperty morphologyClosingMaskSizeProperty() {
        return morphologyClosingMaskSize;
    }

    public static Integer getMorphologyClosingMaskSize() {
        return morphologyClosingMaskSize.getValue();
    }

    public static void setMorphologyClosingMaskSize(int value) {
        morphologyClosingMaskSize.set(value);
    }

    public static IntegerProperty verticalLengthHistMaxLengthToTrackProperty() {
        return verticalLengthHistMaxLengthToTrack;
    }

    public static Integer getVerticalLengthHistMaxLengthToTrack() {
        return verticalLengthHistMaxLengthToTrack.getValue();
    }

    public static void setVerticalLengthHistMaxLengthToTrack(int value) {
        verticalLengthHistMaxLengthToTrack.set(value);
    }

    public static DoubleProperty houghRhoProperty() {
        return houghRho;
    }

    public static Double getHoughRho() {
        return houghRho.getValue();
    }

    public static void setHoughRho(double value) {
        houghRho.set(value);
    }

    public static DoubleProperty houghThetaProperty() {
        return houghTheta;
    }

    public static Double getHoughTheta() {
        return houghTheta.getValue();
    }

    public static void setHoughTheta(double value) {
        houghTheta.set(value);
    }

    public static IntegerProperty houghThresholdProperty() {
        return houghThreshold;
    }

    public static Integer getHoughThreshold() {
        return houghThreshold.getValue();
    }

    public static void setHoughThreshold(int value) {
        houghThreshold.set(value);
    }

    public static IntegerProperty houghMinLineLengthProperty() {
        return houghMinLineLength;
    }

    public static Integer getHoughMinLineLength() {
        return houghMinLineLength.getValue();
    }

    public static void setHoughMinLineLength(int value) {
        houghMinLineLength.set(value);
    }

    public static IntegerProperty houghMaxLineGapProperty() {
        return houghMaxLineGap;
    }

    public static Integer getHoughMaxLineGap() {
        return houghMaxLineGap.getValue();
    }

    public static void setHoughMaxLineGap(int value) {
        houghMaxLineGap.set(value);
    }

    public static IntegerProperty houghMaxSegmentAngleProperty() {
        return houghMaxSegmentAngle;
    }

    public static Integer getHoughMaxSegmentAngle() {
        return houghMaxSegmentAngle.getValue();
    }

    public static void setHoughMaxSegmentAngle(int value) {
        houghMaxSegmentAngle.set(value);
    }

    public static IntegerProperty noteHeadWindowingStepProperty() {
        return noteHeadWindowingStep;
    }

    public static Integer getNoteHeadWindowingStep() {
        return noteHeadWindowingStep.getValue();
    }

    public static void setNoteHeadWindowingStep(int value) {
        noteHeadWindowingStep.set(value);
    }
    
    
    public static DoubleProperty bboxUnificationThreshold() {
        return bboxUnificationThreshold;
    }

    public static Double getBboxUnificationThreshold() {
        return bboxUnificationThreshold.getValue();
    }

    public static void setBboxUnificationThreshold(double value) {
        bboxUnificationThreshold.set(value);
    }

}
