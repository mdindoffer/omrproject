package eu.dindoffer.omr;

import java.io.File;

/**
 * We cannot use the {@link FxMain} directly on JDK 11 when fat-jaring JFX. The main java class must not extend
 * the Fx Application, so a POJO Main, behold! https://stackoverflow.com/questions/52653836/maven-shade-javafx-runtime-components-are-missing
 */
public class Main {

    public static void main(String[] args) {
        //load the OS specific OpenCV native library
        if (System.getProperty("os.name").equals("Linux")) {
            System.load(System.getProperty("user.dir") + File.separator + "native" + File.separator + "opencv_linux-3.4.1-64.so");
        } else {
            switch (System.getProperty("sun.arch.data.model")) {
                case "64":
                    System.load(System.getProperty("user.dir") + "\\native\\opencv_win-3.4.1-64.dll");
                    break;
                case "32":
                    System.load(System.getProperty("user.dir") + "\\native\\opencv_win-3.4.1-32.dll");
                    break;
                default:
                    System.err.println("Unresolved platform bitness! OpenCV native library will not be loaded!");
            }
        }
        FxMain.main(args);
    }
}
